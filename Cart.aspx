﻿<%@ Page Title="Cart" Language="C#" AutoEventWireup="true" MasterPageFile="~/Site.Master"  CodeFile="Cart.aspx.cs" Inherits="Cart" %>

<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="MainContent">
    <style>
        :root{ --main-color: #df5f16; }

        .product-content .title{
            margin-top:40px;
            text-align:center;
            font-family:Roboto;
            font-size:12px;
            text-transform:uppercase;
        }

        .feature-title{
            font-weight:600;
            color:#000;
        }

        .feature-title h2{
            margin-top:60px;
            margin-bottom:50px;
            color: #000;
            font-family: Roboto;
            font-size: 38px;
            letter-spacing: 1px;
            text-transform: uppercase;
            font-weight: 700;
        }

        .price, .rating{
            display:inline-block;
            margin:0 auto;
            width:100%;
            text-align:center;
        }

        .product-grid{
            border:1px solid transparent;
            font-family: 'Roboto', sans-serif;
            padding:10px 10px;
            transition: all 0.5s;
            margin-bottom:80px;
            margin-top:20px;
        }

        .product-grid .product-image{
            position: relative;
            width: 15rem;
            height: 10rem;
            overflow: hidden;
        }
        .product-grid .product-image a.image{ display: block; }
        .product-grid .product-image img{
            width: 100%;
            height: auto;
          border-radius: 5px;
   
        }
        .product-grid .add-to-cart{
            color: #fff;
            background: #222;
            font-size: 34px;
            font-weight: 500;
            text-align: center;
            padding: 0 5px;
            display: block;
            position: absolute;
            bottom: -50px;
            right: 0;
            transition: all ease-in-out 0.35s;
            width:20%;
            opacity: 0;

        }
        .product-grid:hover .add-to-cart{ opacity:1; }
        .product-grid .add-to-cart:after{
            content: "\f101";
            color: #fff;
            font-family: 'Font Awesome 5 Free';
            font-weight: 900;
            display: inline-block;
            opacity: 0;
            transition: all 0.5s;
    
        }
        .product-grid .add-to-cart:hover:after{
            padding-left: 10px;
            opacity: 1;
        }
        .product-grid .product-content{ position: relative; }
        .product-grid .product-stock{
            color: #999;
            font-size: 16px;
            font-weight: 600;
            line-height: 35px;
            border-bottom: 1px solid #c1c1c1;
            display: block;
            margin: 0 0 15px;
        }
        .product-grid .icon{
            padding: 0;
            margin: 0;
            list-style: none;
            opacity: 0;
            position: absolute;
            top: -15px;
            right: 0;
            transition: all ease-in-out 0.35s;
        }
        .product-grid:hover .icon{
            opacity: 1;
            top: 8px;
        }
        .product-grid .icon li{ display: inline-block; }
        .product-grid .icon li a{
            color: #222;
            font-size: 16px;
            margin: 0 4px;
        }
        .product-grid .icon li a:hover{ color: var(--main-color); }
        .product-grid .title{
             font-family:Roboto;
            font-size:15px;
            text-transform:uppercase;
            font-weight:700;
            line-height:2px;
        }
        .product-grid .title a{
            color: #222;
            transition: all 0.5s ease-out 0s;
        }
        .product-grid .title a:hover{ color: var(--main-color); }
        .product-grid .category{
            font-size: 18px;
            margin: 0 0 3px;
            display: block;
        }
        .product-grid .category a{
            color: #222;
            transition: all 0.3s ease 0s;
        }
        .product-grid .category a:hover{ color: var(--main-color); }
        .product-grid .price{
            font-family:Roboto;
            font-size:14px;
            text-transform:uppercase;
            font-weight:700;
            line-height:0px;
            display: inline-block;
            color:#000;
        }
        .product-grid .rating{
            padding: 0;
            margin: 0;
            list-style: none;
            float: right;
        }
        .product-grid .rating li{
            color: #FFB14B;
            font-size: 13px;
            display: inline-block;
        }


        .product-grid .rating li.disable{ color: #c1c1c1; }
        @media only screen and (max-width:990px){
            .product-grid{ margin-bottom: 30px; }
        }
    </style>

    <div class="container">
        <div class="col-md-12 text-center feature-title">
            <h2>YOUR CART</h2>
        </div>
        <div class="row">
            <div class="col-3 pt-4 pr-4">
                <h5 class="text-center mb-3">ORDER SUMMARY</h5>
                <table class="table ">
                    <tbody>
                        <tr>
                            <th>Subtotal</th>
                            <td></td>
                            <td data-bind="text: '₹'+subtotal()"></td>
                        </tr>
                        <tr>
                            <th>Tax</th>
                            <td>7%</td>
                            <td data-bind="text: '₹'+tax()"></td>
                        </tr>
                        <tr>
                            <th scope="row">Total</th>
                            <td>=</td>
                            <td data-bind="text: '₹'+totalPrice()"></td>
                        </tr>
                    </tbody>
                </table>
                <!-- ko if: noProducts -->
                    <div class="text-center">
                        <a class="mx-auto btn btn-info disable disabled " href="/Courses/Billing">Proceed To Checkout</a>
                    </div>
                <!-- /ko -->
                <!-- ko ifnot: noProducts -->
                    <div class="text-center">
                        <a class="mx-auto btn btn-info" href="/Courses/Billing">Proceed To Checkout</a>
                    </div>
                <!-- /ko -->
            </div>
            <!-- ko if: noProducts -->
            <div class="col-9">
                <div class="text-center">
                    <h5>
                        No items in your cart !
                    </h5>
                    <a href="/Courses/List" class="btn btn-outline-info">Add Items</a>
                </div>
            </div>
            <!-- /ko -->
            <!-- ko ifnot: noProducts -->
            <div class="col-9">
                <div class="row course-showcase" data-bind="foreach: { data: courses, as: 'course' }">
                    <div class="col-4">
                        <div class="product-grid">
                            <div class="product-image">
                                <img class="pic-1" data-bind="attr: { src: course.src }">
                            </div>
                            <div class="product-content">
                                <h3 class="title"><a data-bind="attr: { href: 'Courses/Details?courseId='+course.courseId }, text: course.productTitle">Product Name</a></h3>
                                <div class="price" data-bind="text: '₹'+course.price"></div>
                            </div>

                            <div class="action-buttons text-center mt-2">
                                <button type="button" data-bind="click: $parent.removeProduct" class="ml-2 btn btn-outline-danger">REMOVE</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /ko -->

        </div>


    </div>           


    <script>
        var memberViewModel = new function () {
            current = this;
            current.noProducts = ko.observable(false);
            current.courses = ko.observableArray();
            current.removeProduct = function (course) {
                if (confirm("Sure?")) {
                    removeFromCart(course.id);
                    memberViewModel.courses.remove(course);
                    let coursesTemp = ko.toJS(memberViewModel.courses);
                    if (coursesTemp.length == 0)
                            memberViewModel.noProducts(true);
                }

            }

            current.subtotal = ko.computed(function () {
                courses = ko.toJS(current.courses);
                let total = 0;
                courses.forEach(function ({ price }) {
                    total+= (Number(price));
                });
                return total;
            });
            current.tax = ko.computed(function () {
                return (current.subtotal() * 7)/100;
            });
            current.totalPrice = ko.computed(function () {
                return (current.subtotal() + current.tax());
            });
        }
        ko.applyBindings(memberViewModel);
        setCourses();
        function removeFromCart(id) {
            $.ajax({
                type: "POST",
                url: "CoursesService.asmx/DeleteFromCartByID",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                data: "{'ID':'"+id+"'}",
                success: function (r) {

                },
                error: function (r) {
                    alert('error');
                    console.error(r.responseText);
                },
                failure: function (r) {
                    alert('failure');
                    console.failure(r.responseText);
                }
            });

        }
        function setCourses() {
            const Username = '<%:Session["user"]%>';
            if(Username!='')
            $.ajax({
                type: "POST",
                url: "CoursesService.asmx/GetCartProducts",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                data: "{'Username':'"+Username+"'}",
                success: function (r) {
                    var array = JSON.parse(r.d);
                    for (let i = 0; i < array.length; i++) {
                        var obj = {
                            id:             array[i]["ID"],
                            courseId:             array[i]["CourseID"],
                            productTitle:    (array[i]["ProductTitle"]),
                            productId: (array[i]["ProductID"]),
                            price:   (array[i]["Price"]),
                            src: '../assets/pic' + i % 3 + '.jpg'
                        }
                        memberViewModel.courses.push(obj);
                    }
                    if (array.length == 0)
                        memberViewModel.noProducts(true);
                },
                error: function (r) {
                    alert('error');
                    console.error(r.responseText);
                },
                failure: function (r) {
                    alert('failure');
                    console.failure(r.responseText);
                }
            });
        }

    </script>
</asp:Content>