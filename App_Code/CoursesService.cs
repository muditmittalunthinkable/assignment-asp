﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.Services;

/// <summary>
/// Summary description for Courses
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
[System.Web.Script.Services.ScriptService]
public class CoursesService : System.Web.Services.WebService
{
    SqlConnection myConnection =
        new SqlConnection(ConfigurationManager.ConnectionStrings["dbconnection"].ConnectionString);
    Guid guid;

    public CoursesService()
    {
        guid = Guid.NewGuid();
        Console.WriteLine(guid.ToString());
        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    [WebMethod]
    public string Create
    (string Username, string Password, string Firstname, string Lastname, string Email,
    string Gender, string Dob, string Specialisation, string Question, string Answer, string CreatedBy)
    {
        myConnection.Open();

        MembershipCreateStatus createStatus;
        MembershipUser user = Membership.CreateUser(
            Username, Password, Email,
            Question, Answer, true, out createStatus);


        string query = "INSERT INTO[dbo].[UserAccount]" +
          "([Username]" +
          ",[Firstname] " +
          ",[Lastname]  " +
          ",[Email]     " +
          ",[Gender]    " +
          ",[Dob]       " +
          ",[Specialisation]" +
          ",[CreatedBy]" +
          ")    " +
          "  VALUES         " +
          "(" +
          "'" + Username + "'," +
          "'" + Firstname + "'," +
          "'" + Lastname + "'," +
          "'" + Email + "'," +
          "'" + Gender + "'," +
          "'" + Dob + "'," +
          "'" + Specialisation + "'," +
          "'" + CreatedBy + "')";

        if (createStatus == MembershipCreateStatus.Success)
        {
            SqlCommand insertCommand = new SqlCommand(query, myConnection);
            if (insertCommand.ExecuteNonQuery() > 0)
            {
                myConnection.Close();
                Roles.AddUserToRole(Username, "Member");
                return "The user account was successfully created";
            }
            else
            {
                Membership.DeleteUser(Username, true);
                return "Some failure";
            }
        }
        else
        {
            return "" + createStatus;
        }

    }

    [WebMethod]
    public string CountAllCourses()
    {

        myConnection.Open();
        string query = "Select Count(ID) from [dbo].[Course] ";
        SqlCommand sqlCommand = new SqlCommand(query, myConnection);
        var reader = sqlCommand.ExecuteReader();
        string temp = "";
        if (reader.HasRows)
        {
            for (int i = 0; reader.Read(); i++)
            {
                temp = reader[0].ToString();
            }
        }
        return temp;
    }


    [WebMethod]
    public string GetAllCourses(string Username, string recordFrom, string recordTo)
    {
        myConnection.Open();
        string query = "Select * from " +
            "(Select [ID]"
            +",[Title]"
            +",[Instructor]"
            +",[Price]"
            +",[Rating], [ProductID] " 
            +"	  ,[InCart] = CASE When EXISTS(SELECT ProductId "
            +"                    FROM[AspMembership].[dbo].[UserCart] UC"
            +"                  WHERE C.ProductID = UC.ProductID AND UC.Username = '" + Username + "') then 1 else 0 end"
            +"	  ,[InPurchase] = CASE When EXISTS(SELECT ProductId"
            +"                    FROM[AspMembership].[dbo].[UserPurchase] UC"
            + "                   WHERE C.ProductID = UC.ProductID AND UC.Username = '" + Username + "') then 1 else 0 end"
            + "    , ROW_NUMBER() OVER(ORDER BY ID) Rownum" +
            "  from [dbo].[Course] C) T2 where Rownum between " + recordFrom + " and " + recordTo;
        // query = "Select Tab.ID, Tab.Username, Tab.Firstname, Tab.Lastname, Tab.Email, Tab.Gender, Tab.Dob, Tab.Specialisation, Tab.CreatedBy from [dbo].[UserAccount] as Tab, (SELECT ROW_NUMBER() OVER(ORDER BY ID) AS RowNum FROM [dbo].[UserAccount]) as tabw Where tabw.RowNum Between 10 and 20";
        SqlCommand sqlCommand = new SqlCommand(query, myConnection);

        string temp = "[";
        using (var reader = sqlCommand.ExecuteReader())
        {
            if (reader.HasRows)
            {
                for (int i = 0; reader.Read(); i++)
                {
                    temp += "" +
                        "{ \"ID\": \"" + reader[0] +
                        "\",\"Title\": \"" + reader[1] +
                        "\",\"Instructor\": \"" + reader[2] +
                        "\",\"Price\": \"" + reader[3] +
                        "\",\"Rating\": \"" + reader[4] +
                        "\",\"ProductID\": \"" + reader[5] +
                        "\",\"InCart\": \"" + reader[6] +
                        "\",\"InPurchase\": \"" + reader[7] +
                        "\",\"Rownum\": \"" + reader[8] +
                        "\"},";
                }
            }
        }
        temp = temp.Substring(0, temp.Length - 1) + "]";
        return temp;
    }

    [WebMethod]
    public string GetCourseByID(string ID, string Username)
    {
        myConnection.Open();
        string query = "Select *" 
            +"	  ,[InCart] = CASE When EXISTS(SELECT ProductId "
            + "                    FROM[AspMembership].[dbo].[UserCart] UC"
            + "                  WHERE C.ProductID = UC.ProductID AND UC.Username = '" + Username + "') then 1 else 0 end"
            + "	  ,[InPurchase] = CASE When EXISTS(SELECT ProductId"
            + "                    FROM[AspMembership].[dbo].[UserPurchase] UC"
             + "                   WHERE C.ProductID = UC.ProductID AND UC.Username = '" + Username + "') then 1 else 0 end"
            +" from [dbo].[Course] C where ID=" +ID;
        // query = "Select Tab.ID, Tab.Username, Tab.Firstname, Tab.Lastname, Tab.Email, Tab.Gender, Tab.Dob, Tab.Specialisation, Tab.CreatedBy from [dbo].[UserAccount] as Tab, (SELECT ROW_NUMBER() OVER(ORDER BY ID) AS RowNum FROM [dbo].[UserAccount]) as tabw Where tabw.RowNum Between 10 and 20";
        SqlCommand sqlCommand = new SqlCommand(query, myConnection);

        string temp ="";
        using (var reader = sqlCommand.ExecuteReader())
        {

            if (reader.HasRows)
            {
                for (int i = 0; reader.Read(); i++)
                {
                    temp += "" +
                        "{ \"ID\": \"" + reader[0] +
                        "\",\"Title\": \"" + reader[1] +
                        "\",\"Instructor\": \"" + reader[2] +
                        "\",\"Price\": \"" + reader[3] +
                        "\",\"Rating\": \"" + reader[4] +
                        "\",\"Category\": \"" + reader[5] +
                        "\",\"Language\": \"" + reader[6] +
                        "\",\"Requirements\": \"" + reader[7] +
                        "\",\"Description\": \"" + reader[8] +
                        "\",\"Length\": \"" + reader[9] +
                        "\",\"Perks\": \"" + reader[10] +
                        "\",\"ProductID\": \"" + reader[11] +
                        "\",\"InCart\": \"" + reader[12] +
                        "\",\"InPurchase\": \"" + reader[13] +
                        "\"}";
                }
            }
        }
        return temp;
    }

    [WebMethod]
    public string AddToCart(string Username, string CourseID, string ProductTitle, string ProductID, string Price)
    {
        string query = "INSERT INTO [dbo].[UserCart]"
                      +"([Username]    "
                      +",[CourseID]    "
                      +",[ProductTitle]"
                      +",[ProductID]    "
                      +",[Price])       "
                      +" VALUES         "
                      + "( '" + Username + "'         "
                      + ", '" + CourseID + "'         "
                      + ", '" + ProductTitle + "'     "
                      + ", '" + ProductID + "'        "
                      + ", '" + Price + "'            "
                      + ")";
        myConnection.Open();
        SqlCommand sqlCommand = new SqlCommand(query, myConnection);
        if (sqlCommand.ExecuteNonQuery() > 0)
        {
            myConnection.Close();
            return "The product was successfully added to cart";
        }
        else
            return "Some failure";
    }

    [WebMethod]
    public string GetCartProducts( string Username )
    {
        myConnection.Open();
        string query = "SELECT * FROM [AspMembership].[dbo].[UserCart] where Username ='"+ Username+"'";

        SqlCommand sqlCommand = new SqlCommand(query, myConnection);

        string temp = "[";
        using (var reader = sqlCommand.ExecuteReader())
        {

            if (reader.HasRows)
            {
                for (int i = 0; reader.Read(); i++)
                {
                    temp += "" +
                        "{ \"ID\": \"" + reader[0] +
                        "\",\"Username\": \"" + reader[1] +
                        "\",\"CourseID\": \"" + reader[2] +
                        "\",\"ProductTitle\": \"" + reader[3] +
                        "\",\"ProductID\": \"" + reader[4] +
                        "\",\"Price\": \"" + reader[5] +
                        "\"},";
                }
            }
        }
        if (temp.Length > 1)
            temp = temp.Substring(0, temp.Length - 1) + "]";
        else
            temp += "]";
        return temp;
    }

    [WebMethod]
    public string DeleteFromCartByID(string ID)
    {
        myConnection.Open();
        string query = "Delete from [dbo].[UserCart] where ID=" + ID;

        SqlCommand sqlCommand = new SqlCommand(query, myConnection);
        if (sqlCommand.ExecuteNonQuery() > 0)
        {
            myConnection.Close();
            return "The item was successfully removed";
        }
        else
            return "Some failure";
    }


    [WebMethod]
    public string AddToBill(string BillDate, string CustomerUsername, string CustomerName, 
        string Subtotal, string Tax, string TotalPrice, string NumberOfProducts, string MobileNumber, string EmailID)
    {
        string query = "" +
                "INSERT INTO [dbo].[BillDetails]" +
               "([BillDate]        " +
               ",[CustomerUsername]" +
               ",[CustomerName]    " +
               ",[Subtotal]        " +
               ",[Tax]             " +
               ",[TotalPrice]      " +
               ",[NumberOfProducts]" +
               ",[MobileNumber]    " +
               ",[EmailID])        " +
               "  VALUES           " +
               "('"+BillDate          +"'" +
               ",'"+ CustomerUsername +"'" +
               ",'"+ CustomerName     +"'" +
               ",'"+ Subtotal         +"'" +
               ",'"+ Tax              +"'" +
               ",'"+ TotalPrice       +"'" +
               ",'"+ NumberOfProducts +"'" +
               ",'"+ MobileNumber     +"'" +
               ",'"+ EmailID          +"')";
        myConnection.Open();
        SqlCommand sqlCommand = new SqlCommand(query, myConnection);
        int f = 0;
        if (sqlCommand.ExecuteNonQuery() > 0)
        {
            f = 1;
        }
        if (f == 1)
        {
            query = "select Top 1 BillID from [AspMembership].[dbo].[BillDetails] order by BillID desc";
            sqlCommand = new SqlCommand(query, myConnection);

            using (var reader = sqlCommand.ExecuteReader()){
                if (reader.HasRows){
                    for (int i = 0; reader.Read(); i++){
                        return reader[0].ToString();
                    }
                }
            }
        }
        return "Some failure";
    }

    [WebMethod]
    public string AddToPurchase(string Username, string CourseID, 
        string ProductTitle, string ProductID, string Price, string PurchaseDate, string BillID)
    {
        string query = "INSERT INTO [dbo].[UserPurchase]"
                      + "([Username]    "
                      + ",[CourseID]    "
                      + ",[ProductTitle]"
                      + ",[ProductID]    "
                      + ",[Price]" 
                      + ",[PurchaseDate]" 
                      + ",[BillID])       "
                      + " VALUES         "
                      + "( '" + Username + "'         "
                      + ", '" + CourseID + "'         "
                      + ", '" + ProductTitle + "'     "
                      + ", '" + ProductID + "'        "
                      + ", '" + Price + "'            "
                      + ", '" + PurchaseDate + "'            "
                      + ", '" + BillID + "'            "
                      + ")";
        myConnection.Open();
        SqlCommand sqlCommand = new SqlCommand(query, myConnection);
        if (sqlCommand.ExecuteNonQuery() > 0)
        {
            myConnection.Close();
            return "Success";
        }
        else
            return "Some failure";
    }

    [WebMethod]
    public string CountPurchaseCoursesByUsername(string Username)
    {
        myConnection.Open();
        string query = "Select Count(ID) from [AspMembership].[dbo].[UserPurchase] where Username='"+Username+"'";
        SqlCommand sqlCommand = new SqlCommand(query, myConnection);
        var reader = sqlCommand.ExecuteReader();
        string temp = "";
        if (reader.HasRows)
        {
            for (int i = 0; reader.Read(); i++)
            {
                temp = reader[0].ToString();
            }
        }
        return temp;
    }


    [WebMethod]
    public string GetPurchaseCoursesByUsername(string Username, string recordFrom, string recordTo)
    {
        myConnection.Open();
        string query = "Select * from " +
            "(Select [ID]"
            + ",[Username]"
            + ",[CourseID]"
            + ",[ProductTitle]"
            + ",[ProductID] "
            + ",[Price] "
            + ",[PurchaseDate] "
            + ",[BillID] "
            + "    , ROW_NUMBER() OVER(ORDER BY ID) Rownum" +
            "  from [dbo].[UserPurchase] where Username = '"+Username+"') T2 where Rownum between " + recordFrom + " and " + recordTo;
        // query = "Select Tab.ID, Tab.Username, Tab.Firstname, Tab.Lastname, Tab.Email, Tab.Gender, Tab.Dob, Tab.Specialisation, Tab.CreatedBy from [dbo].[UserAccount] as Tab, (SELECT ROW_NUMBER() OVER(ORDER BY ID) AS RowNum FROM [dbo].[UserAccount]) as tabw Where tabw.RowNum Between 10 and 20";
        SqlCommand sqlCommand = new SqlCommand(query, myConnection);

        string temp = "[";
        using (var reader = sqlCommand.ExecuteReader())
        {
            if (reader.HasRows)
            {
                for (int i = 0; reader.Read(); i++)
                {
                    temp += "" +
                        "{ \"ID\": \"" + reader[0] +
                        "\",\"Username\": \"" + reader[1] +
                        "\",\"CourseID\": \"" + reader[2] +
                        "\",\"ProductTitle\": \"" + reader[3] +
                        "\",\"ProductID\": \"" + reader[4] +
                        "\",\"Price\": \"" + reader[5] +
                        "\",\"PurchaseDate\": \"" + reader[6] +
                        "\",\"BillID\": \"" + reader[7] +
                        "\",\"Rownum\": \"" + reader[8] +
                        "\"},";
                }
            }
        }
        if (temp.Length > 1)
            temp = temp.Substring(0, temp.Length - 1) + "]";
        else
            temp += "]";
        return temp;
    }



}
