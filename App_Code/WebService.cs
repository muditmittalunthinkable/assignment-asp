﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Web;
using System.Web.Http;
using System.Web.Script.Services;
using System.Web.Security;
using System.Web.Services;

/// <summary>
/// Summary description for WebService
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
 [System.Web.Script.Services.ScriptService]
public class WebService : System.Web.Services.WebService
{

    SqlConnection myConnection = 
        new SqlConnection(ConfigurationManager.ConnectionStrings["dbconnection"].ConnectionString);

    public WebService()
    {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    [WebMethod]
    public string Create
    (string Username, string Password, string Firstname, string Lastname, string Email,
        string Gender, string Dob, string Specialisation, string Question, string Answer, string CreatedBy)
    {
        myConnection.Open();

        MembershipCreateStatus createStatus;
        MembershipUser user = Membership.CreateUser(
            Username, Password, Email,
            Question, Answer, true, out createStatus);


        string query = "INSERT INTO[dbo].[UserAccount]" +
          "([Username]" +
          ",[Firstname] " +
          ",[Lastname]  " +
          ",[Email]     " +
          ",[Gender]    " +
          ",[Dob]       " +
          ",[Specialisation]" +
          ",[CreatedBy]" +
          ")    " +
          "  VALUES         " +
          "(" +
          "'"+Username+"'," +
          "'"+Firstname+"'," +
          "'"+Lastname+"'," +
          "'"+Email+"'," +
          "'"+Gender+"'," +
          "'"+Dob+"'," +
          "'"+Specialisation +"',"+
          "'"+CreatedBy +"')";

        if(createStatus == MembershipCreateStatus.Success)
        {
            SqlCommand insertCommand = new SqlCommand(query, myConnection);
            if (insertCommand.ExecuteNonQuery() > 0)
            {
                myConnection.Close();
                Roles.AddUserToRole(Username, "Member");
                return "The user account was successfully created";
            }
            else
            {
                Membership.DeleteUser(Username, true);
                return "Some failure";
            }

        }
        else
        {
            return ""+createStatus;
        }

    }

    [WebMethod]
    public string Update
        (string ID, string Firstname, string Lastname, 
        string Gender, string Dob, string Specialisation, string CreatedBy)
    {
        
        myConnection.Open();

        string query = "Update [dbo].[UserAccount] " +
            "Set Firstname = '" + Firstname + "'," +
            "Lastname = '" + Lastname+ "'," +
            "Gender ='" + Gender + "'," +
            "Dob='" + Dob + "'," +
            "Specialisation='"+Specialisation+"'," +
            "CreatedBy ='" + CreatedBy + "'" +
            "where ID="+ID;
            

        SqlCommand updateCommand = new SqlCommand(query, myConnection);
        if (updateCommand.ExecuteNonQuery() > 0)
        {
            myConnection.Close();
            return "The user account was successfully updated" ;
        }
        else
            return "Some failure";
            
    }

    [WebMethod]
    public string DeleteByID(string ID)
    {
        myConnection.Open();
        string query = "Select Username from [dbo].[UserAccount] where ID=" + ID;

        SqlCommand sqlCommand = new SqlCommand(query, myConnection);
        var reader = sqlCommand.ExecuteReader();
        string uN = "";
        if (reader.HasRows)
        {
            for (int i = 0; reader.Read(); i++)
            {
                uN = reader[0].ToString();
            }
        }
        else
        {
            return "Some failure";
        }
        reader.Close();
        return DeleteByUN(uN);
    }
    public string DeleteByUN(string uN)
    {
        string query2 = "Delete from [dbo].[UserAccount] where Username='" + uN+"'";

        SqlCommand sqlCommand2 = new SqlCommand(query2, myConnection);
        if (sqlCommand2.ExecuteNonQuery() > 0)
        {
            myConnection.Close();
            Membership.DeleteUser(uN, true);
            return "The user account was successfully deleted";
        }
        else
            return "Some failure";
    }

    [WebMethod]
    public string ReadByID(string ID)
    {
        myConnection.Open();

        string query = "Select * from [dbo].[UserAccount] where ID=" + ID;

        SqlCommand sqlCommand = new SqlCommand(query, myConnection);
        var jsonResult = new StringBuilder();
        var reader = sqlCommand.ExecuteReader();
        string temp = "";
        if (reader.HasRows)
        {
            for (int i=0;  reader.Read(); i++)
            {
                temp += "" +
                    "{ \"ID\": \"" + reader[0] +
                    "\",\"Username\": \"" + reader[1] +
                    "\",\"Firstname\": \"" + reader[2] +
                    "\",\"Lastname\": \"" + reader[3] +
                    "\",\"Email\": \"" + reader[4] +
                    "\",\"Gender\": \"" + reader[5] +
                    "\",\"Dob\": \"" + reader[6] +
                    "\",\"Specialisation\": \"" + reader[7] +
                    "\",\"CreatedBy\": \"" + reader[8]+ "\"},";
            }
        }
        temp = temp.Substring( 0, temp.Length -1) +  "";
        return temp;
    }

    [WebMethod]
    public string ReadByUsername(string Username)
    {
        myConnection.Open();

        string query = "Select * from [dbo].[UserAccount] where Username='" + Username+"'";

        SqlCommand sqlCommand = new SqlCommand(query, myConnection);
        var jsonResult = new StringBuilder();
        var reader = sqlCommand.ExecuteReader();
        string temp = "";
        if (reader.HasRows)
        {
            for (int i = 0; reader.Read(); i++)
            {
                temp += "" +
                    "{ \"ID\": \"" + reader[0] +
                    "\",\"Username\": \"" + reader[1] +
                    "\",\"Firstname\": \"" + reader[2] +
                    "\",\"Lastname\": \"" + reader[3] +
                    "\",\"Email\": \"" + reader[4] +
                    "\",\"Gender\": \"" + reader[5] +
                    "\",\"Dob\": \"" + reader[6] +
                    "\",\"Specialisation\": \"" + reader[7] +
                    "\",\"CreatedBy\": \"" + reader[8] + "\"},";
            }
        }
        temp = temp.Substring(0, temp.Length - 1) + "";
        return temp;
    }


    [WebMethod]
    public string ReadAll(string recordFrom, string recordTo)
    {
        myConnection.Open();
        string query = "Select * from (Select *, ROW_NUMBER() OVER( ORDER BY ID) Rownum from [dbo].[UserAccount]) T2 where Rownum between "+recordFrom+" and "+recordTo;
       // query = "Select Tab.ID, Tab.Username, Tab.Firstname, Tab.Lastname, Tab.Email, Tab.Gender, Tab.Dob, Tab.Specialisation, Tab.CreatedBy from [dbo].[UserAccount] as Tab, (SELECT ROW_NUMBER() OVER(ORDER BY ID) AS RowNum FROM [dbo].[UserAccount]) as tabw Where tabw.RowNum Between 10 and 20";
        SqlCommand sqlCommand = new SqlCommand(query, myConnection);
        
        string temp = "[";
        using (var reader = sqlCommand.ExecuteReader())
        {

            if (reader.HasRows)
            {
                for (int i = 0; reader.Read(); i++)
                {
                    temp += "" +
                        "{ \"ID\": \"" + reader[0] +
                        "\",\"Username\": \"" + reader[1] +
                        "\",\"Firstname\": \"" + reader[2] +
                        "\",\"Lastname\": \"" + reader[3] +
                        "\",\"Email\": \"" + reader[4] +
                        "\",\"Gender\": \"" + reader[5] +
                        "\",\"Dob\": \"" + reader[6] +
                        "\",\"Specialisation\": \"" + reader[7] +
                        "\",\"CreatedBy\": \"" + reader[8] + 
                        "\",\"Rownum\": \"" + reader[9] +
                        "\"},";
                }
            }
        }
        temp = temp.Substring(0, temp.Length - 1) + "]";
        return temp;
    }

    [WebMethod]
    public string Count()
    {
        myConnection.Open();
        string query = "Select Count(ID) from [dbo].[UserAccount] ";
        SqlCommand sqlCommand = new SqlCommand(query, myConnection);
        var reader = sqlCommand.ExecuteReader();
        string temp = "";
        if (reader.HasRows)
        {
            for (int i = 0; reader.Read(); i++)
            {
                temp = reader[0].ToString();
            }
        }
        return temp;
    }

    [WebMethod]
    public string GetMemberDetails(string Username)
    {

        myConnection.Open();
        string query = "select * from [AspMembership].[dbo].[UserAccount], " +
            "(select CreateDate from [AspMembership].[dbo].[aspnet_Membership] " +
            "where UserId = (select UserId from[AspMembership].[dbo].aspnet_Users where UserName ='" + Username + "'"
                        + ")) Table2 where Username = '" + Username + "'";
        SqlCommand sqlCommand = new SqlCommand(query, myConnection);

        string temp = "";
        using (var reader = sqlCommand.ExecuteReader())
        {

            if (reader.HasRows)
            {
                for (int i = 0; reader.Read(); i++)
                {
                    temp = "" +
                        "{ \"ID\": \"" + reader[0] +
                        "\",\"Username\": \"" + reader[1] +
                        "\",\"Firstname\": \"" + reader[2] +
                        "\",\"Lastname\": \"" + reader[3] +
                        "\",\"Email\": \"" + reader[4] +
                        "\",\"Gender\": \"" + reader[5] +
                        "\",\"Dob\": \"" + reader[6] +
                        "\",\"Specialisation\": \"" + reader[7] +
                        "\",\"CreatedBy\": \"" + reader[8] +
                        "\",\"CreateDate\": \"" + reader[9] +
                        "\"}";
                }
            }
        }
        return temp;

    }

    [WebMethod]
    public string GetMemberAddress(string Username)
    {
        myConnection.Open();
        string query = "select * FROM [AspMembership].[dbo].[UserAddress] where UserName = '"+Username+"'";
        SqlCommand sqlCommand = new SqlCommand(query, myConnection);

        string temp = "[";
        using (var reader = sqlCommand.ExecuteReader())
        {

            if (reader.HasRows)
            {
                for (int i = 0; reader.Read(); i++)
                {
                    temp += "" +
                        "{ \"ID\": \"" + reader[0] +
                        "\",\"UserName\": \"" + reader[1] +
                        "\",\"AddressType\": \"" + reader[2] +
                        "\",\"AddressLine1\": \"" + reader[3] +
                        "\",\"AddressLine2\": \"" + reader[4] +
                        "\",\"City\": \"" + reader[5] +
                        "\",\"PinCode\": \"" + reader[6] +
                        "\",\"Country\": \"" + reader[7] +
                        "\",\"State\": \"" + reader[8] +
                        "\",\"Phone\": \"" + reader[9] +
                        "\",\"Fax\": \"" + reader[10] +
                        "\"},";
                }
            }
        }
        temp = temp.Substring(0, temp.Length - 1) + "]";
        return temp;
    }

    [WebMethod]
    public string DeleteAddressByID(string ID)
    {
        myConnection.Open();
        string query2 = "Delete from [dbo].[UserAddress] where ID='" + ID + "'";

        SqlCommand sqlCommand2 = new SqlCommand(query2, myConnection);
        if (sqlCommand2.ExecuteNonQuery() > 0)
        {
            myConnection.Close();
            return "The user address was successfully deleted";
        }
        else
            return "Some failure";
    }

    [WebMethod]
    public string AddNewAddress(string UserName,string AddressType,string AddressLine1,string AddressLine2,string City,string PinCode,string Country,string State,string Phone,string Fax)
    {

        string query = "INSERT INTO[dbo].[UserAddress]" +
                    "([UserName]      " +
                    "  ,[AddressType] " +
                    "  ,[AddressLine1]" +
                    "  ,[AddressLine2]" +
                    "  ,[City]        " +
                    "  ,[PinCode]     " +
                    "  ,[Country]     " +
                    "  ,[State]       " +
                    "  ,[Phone]       " +
                    "  ,[Fax])        " +
                    "VALUES(" +
                    "'"+UserName+"'" +
                    ",'"+AddressType+"'" +
                    ",'"+AddressLine1+"'" +
                    ",'"+AddressLine2+"'" +
                    ",'"+City+"'" +
                    ",'"+PinCode+"'" +
                    ",'"+Country+"'" +
                    ",'"+State+"'" +
                    ",'"+Phone+"'" +
                    ",'"+Fax+"')";

        myConnection.Open();
        SqlCommand sqlCommand2 = new SqlCommand(query, myConnection);
        if (sqlCommand2.ExecuteNonQuery() > 0)
        {
            myConnection.Close();
            return "The user address was successfully added";
        }
        else
            return "Some failure";
    }

    [WebMethod]
    public string UpdateAddressByID(string ID, string UserName, string AddressType, string AddressLine1, string AddressLine2, string City, string PinCode, string Country, string State, string Phone, string Fax)
    {
        string query= "UPDATE[dbo].[UserAddress]" +
            "SET[UserName] =      '"+UserName      +"'" +
            "  ,[AddressType] =   '"+AddressType   +"'" +
            "  ,[AddressLine1] =  '"+AddressLine1  +"'" +
            "  ,[AddressLine2] =  '"+AddressLine2  +"'" +
            "  ,[City] =          '"+City          +"'" +
            "  ,[PinCode] =       '"+PinCode       +"'" +
            "  ,[Country] =       '"+Country       +"'" +
            "  ,[State] =         '"+State         +"'" +
            "  ,[Phone] =         '"+Phone         +"'" +
            "  ,[Fax] =           '"+Fax           +"'" +
            " WHERE ID="+ID;


        myConnection.Open();
        SqlCommand sqlCommand2 = new SqlCommand(query, myConnection);
        if (sqlCommand2.ExecuteNonQuery() > 0)
        {
            myConnection.Close();
            return "The user address was successfully updated";
        }
        else
            return "Some failure";
    }

}
