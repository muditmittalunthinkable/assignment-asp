﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <style>
        a.disabled {
            color: #ccc;
        }
        a.active{
            border: 1px solid grey;
        }
        #MemberTemplate div.col-3{
            margin: 2rem;
            padding: 2rem;
            border: 1px solid grey;
            height: fit-content;
        }
    </style>
                <div class="col-12 text-center">
                <a href="/Courses/List" class="btn btn-outline-info mt-3">Search Courses</a>
            </div>

    <asp:Panel runat="server" ID="AdminPanel">
        <div id="adminTemplate" class="row p-3">
            <a runat="server" href="~/Account/Register2">Register &nbsp;</a> a new User
            <div class="col-12">

            Records on 1 page: 
                <span id="recordsOnOnePage">
                    <select onchange="setPagination()">
                        <option>20</option>
                        <option>25</option>
                        <option>30</option>
                        <option>35</option>
                        <option>40</option>
                        <option>45</option>
                        <option>50</option>
                    </select>
                </span>
            </div>
            
            &nbsp;&nbsp;
            
            <p>Total Records : <span id="totalRecords"></span></p>
            &nbsp;&nbsp;
            
            <p>Total Pages: <span id="totalPages"></span></p>
            &nbsp;&nbsp;
            
            <p>Current Page: <span id="currentPage"></span></p>
            &nbsp;&nbsp;
            
            <p>Records from: <span id="recordFrom"></span> to: <span id="recordTo"></span></p>

            <div class="col-12 text-center">
                <a class="btn" id="prevPagination"><</a>
                <span id="pagination"></span>
                <a class="btn" id="nextPagination">></a>
            </div>

            <table class="table table-striped">
                <thead>
                  <tr>
                      <th scope="col">#</th>
                      <th scope="col">Firstname</th>
                      <th scope="col">Lastname</th>
                      <th scope="col">Email</th>
                      <th scope="col">Username</th>
                      <th scope="col">Gender</th>
                      <th scope="col">Dob</th>
                      <th scope="col">Specialisation</th>
                      <th scope="col">CreatedBy</th>
                      <th scope="col">EDIT</th>
                      <th scope="col">DELETE</th>
                  </tr>
                </thead>
                <tbody>
    
                </tbody>
    
                <asp:PlaceHolder ID="DBDataPlaceHolder" runat="server"></asp:PlaceHolder>  
            </table>
    
    
    
            <!--Modal-->
            <div class="modal fade" id="Modal" tabindex="-1" role="dialog" 
                aria-labelledby="exampleModalLabel" aria-hidden="true">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h3 class="modal-title" id="exampleModalLabel">Edit Fields</h3>
                  </div>
                  <div class="modal-body">
                    <form>
                      <div class="form-group">
                        <label for="firstName" class="col-form-label">First Name</label>
                        <input type="text" class="form-control" id="firstName">
                      </div>
                      <div class="form-group">
                        <label for="lastName" class="col-form-label">Last Name</label>
                        <input type="text" class="form-control" id="lastName">
                      </div>
                      <div class="form-group">
                        <label for="gender" class="col-form-label">Gender</label>
                        <input type="text" class="form-control" id="gender">
                      </div>
                      <div class="form-group">
                        <label for="dob" class="col-form-label">Date of Birth</label>
                        <input type="date" class="form-control" id="dob">
                      </div>
                      <div class="form-group">
                        <label for="specialisation" class="col-form-label">Specialisation</label>
                        <input type="text" class="form-control" id="specialisation">
                      </div>
                      <div class="form-group">
                        <label for="createdBy" class="col-form-label">Created By</label>
                        <input type="text" class="form-control" id="createdBy">
                      </div>
                    </form>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" id="update_Click" class="btn btn-primary">Update</button>
                  </div>
                </div>
              </div>
            </div>
    
    
    
        </div>
    </asp:Panel>

    <asp:Panel runat="server" ID="MemberPanel">
        <div class="row" id="MemberTemplate">
            <div class="col-3">
                <h3>Address Information</h3>
                <!-- ko ifnot: isEditing -->
                    <button class="btn btn-primary" data-bind="click: EditingToggle">Edit</button>
                    <div data-bind="foreach: { data: addresses, as: 'address' }">
                        <div style="margin: 1rem; padding: 1rem">
                            <div>
                                <span style="font-weight: bold" data-bind="text: address.addressType()">
                                </span>:
                                <span data-bind="html: address.addressLine1() + '<br>' +
                                        address.addressLine2() + '<br>' + address.city() + ' -' +
                                        address.pinCode() + '<br>' + address.state() +', '+ address.country()">
                                </span>
                            </div>
                            <div>
                                <span style="font-weight: bold">
                                    Phone
                                </span>:
                                <span data-bind="html: address.phone()">
                                </span>
                            </div>
                            <div>
                                <span style="font-weight: bold">
                                    Fax
                                </span>:
                                <span data-bind="html: address.fax()">
                                </span>
                            </div>
                        </div>
                    </div>
                <!-- /ko -->
                <!-- ko if: isEditing -->
                    <div id="editing template" data-bind="foreach: { data: addresses, as: 'address' }">
                        <div style="padding: 2rem; margin: 1rem; border: 0.5px solid grey">
                            <button class="btn btn-danger" data-bind="click: $parent.removeAddress">Delete</button>
                            <div class="form-group">
                                <label for="addressType" class="col-form-label">Type</label>
                                <select  class="form-control"
                                    data-bind="options: $parents[0].availableAddressTypes,
    value: address.addressType, optionsCaption: 'Choose...'"
                                    ></select>
                            </div>
                            <div class="form-group">
                                <label class="col-form-label">Address Line</label>
                                <input type="text" class="form-control" id="addressLine1" data-bind="value: address.addressLine1">
                                <input type="text" class="form-control" id="addressLine2" data-bind="value: address.addressLine2">
                            </div>
                            <div class="form-group">
                                <label for="city" class="col-form-label">City</label>
                                <input type="text" class="form-control" id="city" data-bind="value: address.city">
                            </div>
                            <div class="form-group">
                                <label for="postalCode" class="col-form-label">Postal Code/Zip</label>
                                <input type="text" class="form-control" id="postalCode" data-bind="value: address.pinCode">
                            </div>
                            <div class="form-group">
                                <label class="col-form-label">Country</label>
                                <select  class="form-control"
                                    data-bind="options: $parents[0].availableCountries, optionsText: 'text',value: address.country,optionsValue: 'text', optionsCaption: 'Choose...'"
                                    ></select>
                            </div>
                            <!-- ko if: address.country() === 'India' -->
                            <div class="form-group">
                                <label for="state" class="col-form-label">State/Province</label>
                                <select  class="form-control"
                                    data-bind="options: $parents[0].availableStates,
                                        value: address.state,
                                        optionsCaption: 'Choose...'"
                                    ></select>
                            </div>
                            <!-- /ko -->
                            <!-- ko ifnot: address.country() === 'India' -->
                            <div class="form-group">
                                <label for="state" class="col-form-label">State/Province</label>
                                <input type="text" class="form-control" id="state" data-bind="value: address.state">
                            </div>
                            <!-- /ko -->
                            <div class="form-group">
                                <label for="phoneNum" class="col-form-label">Phone number</label>
                                <input type="text" class="form-control" id="phoneNum" data-bind="value: address.phone">
                            </div>
                            <div class="form-group">
                                <label for="fax" class="col-form-label">Fax</label>
                                <input type="text" class="form-control" id="fax" data-bind="value: address.fax">
                            </div>
                        </div>
                    </div>
                    <button class="btn" data-bind="click: addAddress">Add address</button>
                    <br />
                    <button class="btn btn-primary" data-bind="click: SaveAddress">Save</button>
                    <button class="btn btn-primary" data-bind="click: EditingToggle">Cancel</button>
                <!-- /ko -->

            </div>
            <div class="col-3">
                <h3>User Details</h3>
                <p>Firstname      : <span data-bind="text: firstName"></span>   </p>
                <p>Lastname       : <span data-bind="text: lastName"></span>   </p>
                <p>Email          : <span data-bind="text: email"></span>   </p>
                <p>Gender         : <span data-bind="text: gender"></span>   </p>
                <p>Dob            : <span data-bind="text: dob"></span>   </p>
                <p>Specialisation : <span data-bind="text: specialisation"></span>   </p>
            </div>
            <div class="col-3">
                <h3>Information</h3>
                <p>Created Date : <span data-bind="text: createdDate"></span>   </p>
                <p>Username       : <span data-bind="text: username"></span>   </p>
                <p>Account Status       : Member   </p>
            </div>


        </div>
    </asp:Panel>

    <script>
        <% 
        if (Roles.GetRolesForUser(Session["user"].ToString()).Contains("Administrator"))
        {
        %>
           setPagination();
           gotoPage(1, $("#recordsOnOnePage option:selected").text());

        <% 
        } 
        %>

        var selectedID = '';
        var totalPages;
        async function setPagination() {
            var recordsOnOnePage = $("#recordsOnOnePage option:selected").text();
            var totalRecords;
            var pages = "";
                await $.ajax({
                    type: "POST",
                    url: "WebService.asmx/Count",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (r) {
                        //alert(r.d);
                        totalRecords = r.d;
                        $("#totalRecords").text(totalRecords);
                        totalPages = parseInt(totalRecords / recordsOnOnePage);
                        if (parseInt(totalRecords / recordsOnOnePage) < (totalRecords / recordsOnOnePage)) {
                            totalPages += 1;
                        }
                        $("#totalPages").text(totalPages);

                    },
                    error: function (r) {
                        alert('error');
                        console.log(r.responseText);
                    },
                    failure: function (r) {
                        alert('failure');
                        console.log(r.responseText);
                    }
                });

            for (let i = 1; i <= totalPages; i++) {
                pages += `<a href="#" class="btn" onclick="gotoPage(${i}, ${recordsOnOnePage}, ${totalRecords})">${i}</a>`;
            }
            $("#pagination").html(pages);
            gotoPage(1, recordsOnOnePage, totalRecords);
        }

        $("#prevPagination").click(function () {
            gotoPage(parseInt($("#currentPage").text()) - 1, $("#recordsOnOnePage option:selected").text());
        })
        $("#nextPagination").click(function () {
            gotoPage(parseInt($("#currentPage").text()) + 1, $("#recordsOnOnePage option:selected").text());
        })

        function gotoPage(pageNum, recordsOnOnePage, totalRecords = 100) {
            var from = ((pageNum - 1) * recordsOnOnePage) + 1;
            var to = (((pageNum - 1) * recordsOnOnePage) + recordsOnOnePage);
            to = to > totalRecords ? totalRecords : to;

            $("#currentPage").text(pageNum);
            $("#recordFrom").text(from);
            $("#recordTo").text(to);
            $(`span#pagination`).children().removeClass("active");
            $(`span#pagination a:nth-child(${pageNum})`).addClass("active");

            if (pageNum == 1)
                $("#prevPagination").addClass("disabled");
            else
                $("#prevPagination").removeClass("disabled");


            if (pageNum == totalPages)
                $("#nextPagination").addClass("disabled");
            else
                $("#nextPagination").removeClass("disabled");



            $.ajax({
                type: "POST",
                url: "WebService.asmx/ReadAll",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                data: "{'recordFrom':'" + from + "', 'recordTo':'" + to + "'}",
                    success: function (r) {
                        var array = JSON.parse(r.d);
                        var htmlTable = "";
                        for (var i = 0; i < array.length; i++) {
                            htmlTable += "<tr>";
                            htmlTable += "<td>" + array[i]["Rownum"] + "</td>";
                            htmlTable += "<td>" + array[i]["Firstname"] + "</td>";
                            htmlTable += "<td>" + array[i]["Lastname"] + "</td>";
                            htmlTable += "<td>" + array[i]["Email"] + "</td>";
                            htmlTable += "<td>" + array[i]["Username"] + "</td>";
                            htmlTable += "<td>" + array[i]["Gender"] + "</td>";
                            htmlTable += "<td>" + array[i]["Dob"].substring(0,10) + "</td>";
                            htmlTable += "<td>" + array[i]["Specialisation"] + "</td>";
                            htmlTable += "<td>" + array[i]["CreatedBy"] + "</td>";
                            htmlTable += (
                                "<td>" +
                                "<p id='edit_btn' onclick='edit_click(" + array[i]["ID"] + ")' class='btn btn-primary'>EDIT</p>" +
                                "</td>");
                            htmlTable += (
                                "<td>" +
                                "<p id='delete_btn' onclick='delete_click(" + array[i]["ID"] + ")' class='btn btn-danger'>DELETE</p>" +
                                "</td>");
                            htmlTable += ("</tr>");
                        }
                        htmlTable += "";
                        $("tbody").html(htmlTable);
                    },
                    error: function (r) {
                        alert('error');
                        console.log(r.responseText);
                    },
                    failure: function (r) {
                        alert('failure');
                        console.log(r.responseText);
                    }
               });

        }

        function edit_click(ID) {
            selectedID = ID;
            console.log("id ",selectedID);
            $("#Modal").modal('show');

            $.ajax({
                    type: "POST",
                    url: "WebService.asmx/ReadByID",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: "{'ID':'"+ID+"'}",
                    success: function (r) {
                        var array = JSON.parse(r.d);
                        console.log(array);
                            $("#firstName").val(array.Firstname);
                            $("#lastName").val(array["Lastname"]);
                        $("#gender").val(array["Gender"]);
                        $("#dob").val(new Date().setFullYear(array["Dob"].substring(0, 2), array["Dob"].substring(3, 5), array["Dob"].substring(6,8) ));
                            $("#specialisation").val(array["Specialisation"]);
                            $("#createdBy").val(array["CreatedBy"]);
                    },
                    error: function (r) {
                        alert('error');
                        console.log(r.responseText);
                    },
                    failure: function (r) {
                        alert('failure');
                        console.log(r.responseText);
                    }
                });
        }

        function delete_click(ID) {
            if (confirm("Are you sure to delete?")) {
                $.ajax({
                    type: "POST",
                    url: "WebService.asmx/DeleteByID",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: "{'ID':'"+ID+"'}",
                    success: function (r) {
                        alert(r.d);
                    },
                    error: function (r) {
                        alert('error');
                        console.log(r.responseText);
                    },
                    failure: function (r) {
                        alert('failure');
                        console.log(r.responseText);
                    }
                });

            }
        }

        $("#update_Click").click(function () {
            console.log("selec id ", selectedID);
            updatedData = "{'ID': '"+selectedID+"','Firstname': '"+$("#firstName").val()+"','Lastname': '"+$("#lastName").val()+"','Gender': '"+$("#gender").val()+"','Dob': '"+$("#dob").val()+"','Specialisation': '"+$("#specialisation").val()+"','CreatedBy': '"+$("#createdBy").val()+"'}";
            $.ajax({
                type: "POST",
                url: "WebService.asmx/Update",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                data: updatedData,
                success: function (r) {
                    $("#Modal").modal('hide');
//                    location.reload();
                    alert('success');
                    alert(r.d);
                },
                error: function (r) {
                    alert('error');
                    console.log(r.responseText);
                },
                failure: function (r) {
                    alert('failure');
                    console.log(r.responseText);
                }
            });

        });


    </script>
    <script>
        <% 
        if (Roles.GetRolesForUser(Session["user"].ToString()).Contains("Member"))
        {
        %>
            setDetails();
            setAddress();
        <% 
        } 
        %>
        var memberViewModel = new function() {
            current= this;
            current.isEditing= ko.observable(false);
            current.firstName= ko.observable("");
            current.lastName= ko.observable("");
            current.email= ko.observable("");
            current.username= ko.observable("");
            current.gender= ko.observable("");
            current.dob= ko.observable("");
            current.specialisation= ko.observable("");
            current.createdDate= ko.observable("");
            current.availableAddressTypes= ["Home", "Office", "Other"];
            current.addresses= ko.observableArray();
            current.addAddress = function () {
                var obj = {
                    id:             ko.observable(null),
                    addressType:    ko.observable(""),
                    addressLine1:   ko.observable(""),
                    addressLine2:   ko.observable(""),
                    city:           ko.observable(""),
                    pinCode:        ko.observable(""),
                    country:        ko.observable(""),
                    state:          ko.observable(""),
                    phone:          ko.observable(""),
                    fax:            ko.observable(""),
                }                   
                current.addresses.push(obj);
            };
            current.removeAddress = function(address) {
                if (confirm("Are you sure to delete?")) {
                    deleteAddress(address);
                }
            };
            current.SaveAddress = function () {
                var jsAddArr = [];
                jsAddArr = ko.toJS(current.addresses);
                console.log(jsAddArr);
                var newAddArr = [];
                var editAddArr = [];
                jsAddArr.forEach((address) => {
                    if (address.id === null) {
                        newAddArr.push(address);
                    }
                    else {
                        editAddArr.push(address);
                    }
                });
                if (newAddArr.length != 0)
                    addNewAddress(newAddArr);
                if (editAddArr.length != 0)
                    editAddress(editAddArr);
            };
            current.availableCountries = [
                { "text": "Afghanistan", "value": "AF" },
                { "text": "Åland Islands", "value": "AX" },
                { "text": "Albania", "value": "AL" },
                { "text": "Algeria", "value": "DZ" },
                { "text": "American Samoa", "value": "AS" },
                { "text": "Andorra", "value": "AD" },
                { "text": "Angola", "value": "AO" },
                { "text": "Anguilla", "value": "AI" },
                { "text": "Antarctica", "value": "AQ" },
                { "text": "Antigua and Barbuda", "value": "AG" },
                { "text": "Argentina", "value": "AR" },
                { "text": "Armenia", "value": "AM" },
                { "text": "Aruba", "value": "AW" },
                { "text": "Australia", "value": "AU" },
                { "text": "Austria", "value": "AT" },
                { "text": "Azerbaijan", "value": "AZ" },
                { "text": "Bahamas", "value": "BS" },
                { "text": "Bahrain", "value": "BH" },
                { "text": "Bangladesh", "value": "BD" },
                { "text": "Barbados", "value": "BB" },
                { "text": "Belarus", "value": "BY" },
                { "text": "Belgium", "value": "BE" },
                { "text": "Belize", "value": "BZ" },
                { "text": "Benin", "value": "BJ" },
                { "text": "Bermuda", "value": "BM" },
                { "text": "Bhutan", "value": "BT" },
                { "text": "Bolivia", "value": "BO" },
                { "text": "Bosnia and Herzegovina", "value": "BA" },
                { "text": "Botswana", "value": "BW" },
                { "text": "Bouvet Island", "value": "BV" },
                { "text": "Brazil", "value": "BR" },
                { "text": "British Indian Ocean Territory", "value": "IO" },
                { "text": "Brunei Darussalam", "value": "BN" },
                { "text": "Bulgaria", "value": "BG" },
                { "text": "Burkina Faso", "value": "BF" },
                { "text": "Burundi", "value": "BI" },
                { "text": "Cambodia", "value": "KH" },
                { "text": "Cameroon", "value": "CM" },
                { "text": "Canada", "value": "CA" },
                { "text": "Cape Verde", "value": "CV" },
                { "text": "Cayman Islands", "value": "KY" },
                { "text": "Central African Republic", "value": "CF" },
                { "text": "Chad", "value": "TD" },
                { "text": "Chile", "value": "CL" },
                { "text": "China", "value": "CN" },
                { "text": "Christmas Island", "value": "CX" },
                { "text": "Cocos (Keeling) Islands", "value": "CC" },
                { "text": "Colombia", "value": "CO" },
                { "text": "Comoros", "value": "KM" },
                { "text": "Congo", "value": "CG" },
                { "text": "Congo, The Democratic Republic of the", "value": "CD" },
                { "text": "Cook Islands", "value": "CK" },
                { "text": "Costa Rica", "value": "CR" },
                { "text": "Cote D'Ivoire", "value": "CI" },
                { "text": "Croatia", "value": "HR" },
                { "text": "Cuba", "value": "CU" },
                { "text": "Cyprus", "value": "CY" },
                { "text": "Czech Republic", "value": "CZ" },
                { "text": "Denmark", "value": "DK" },
                { "text": "Djibouti", "value": "DJ" },
                { "text": "Dominica", "value": "DM" },
                { "text": "Dominican Republic", "value": "DO" },
                { "text": "Ecuador", "value": "EC" },
                { "text": "Egypt", "value": "EG" },
                { "text": "El Salvador", "value": "SV" },
                { "text": "Equatorial Guinea", "value": "GQ" },
                { "text": "Eritrea", "value": "ER" },
                { "text": "Estonia", "value": "EE" },
                { "text": "Ethiopia", "value": "ET" },
                { "text": "Falkland Islands (Malvinas)", "value": "FK" },
                { "text": "Faroe Islands", "value": "FO" },
                { "text": "Fiji", "value": "FJ" },
                { "text": "Finland", "value": "FI" },
                { "text": "France", "value": "FR" },
                { "text": "French Guiana", "value": "GF" },
                { "text": "French Polynesia", "value": "PF" },
                { "text": "French Southern Territories", "value": "TF" },
                { "text": "Gabon", "value": "GA" },
                { "text": "Gambia", "value": "GM" },
                { "text": "Georgia", "value": "GE" },
                { "text": "Germany", "value": "DE" },
                { "text": "Ghana", "value": "GH" },
                { "text": "Gibraltar", "value": "GI" },
                { "text": "Greece", "value": "GR" },
                { "text": "Greenland", "value": "GL" },
                { "text": "Grenada", "value": "GD" },
                { "text": "Guadeloupe", "value": "GP" },
                { "text": "Guam", "value": "GU" },
                { "text": "Guatemala", "value": "GT" },
                { "text": "Guernsey", "value": "GG" },
                { "text": "Guinea", "value": "GN" },
                { "text": "Guinea-Bissau", "value": "GW" },
                { "text": "Guyana", "value": "GY" },
                { "text": "Haiti", "value": "HT" },
                { "text": "Heard Island and Mcdonald Islands", "value": "HM" },
                { "text": "Holy See (Vatican City State)", "value": "VA" },
                { "text": "Honduras", "value": "HN" },
                { "text": "Hong Kong", "value": "HK" },
                { "text": "Hungary", "value": "HU" },
                { "text": "Iceland", "value": "IS" },
                { "text": "India", "value": "IN" },
                { "text": "Indonesia", "value": "ID" },
                { "text": "Iran, Islamic Republic Of", "value": "IR" },
                { "text": "Iraq", "value": "IQ" },
                { "text": "Ireland", "value": "IE" },
                { "text": "Isle of Man", "value": "IM" },
                { "text": "Israel", "value": "IL" },
                { "text": "Italy", "value": "IT" },
                { "text": "Jamaica", "value": "JM" },
                { "text": "Japan", "value": "JP" },
                { "text": "Jersey", "value": "JE" },
                { "text": "Jordan", "value": "JO" },
                { "text": "Kazakhstan", "value": "KZ" },
                { "text": "Kenya", "value": "KE" },
                { "text": "Kiribati", "value": "KI" },
                { "text": "Korea, Democratic People'S Republic of", "value": "KP" },
                { "text": "Korea, Republic of", "value": "KR" },
                { "text": "Kuwait", "value": "KW" },
                { "text": "Kyrgyzstan", "value": "KG" },
                { "text": "Lao People'S Democratic Republic", "value": "LA" },
                { "text": "Latvia", "value": "LV" },
                { "text": "Lebanon", "value": "LB" },
                { "text": "Lesotho", "value": "LS" },
                { "text": "Liberia", "value": "LR" },
                { "text": "Libyan Arab Jamahiriya", "value": "LY" },
                { "text": "Liechtenstein", "value": "LI" },
                { "text": "Lithuania", "value": "LT" },
                { "text": "Luxembourg", "value": "LU" },
                { "text": "Macao", "value": "MO" },
                { "text": "Macedonia, The Former Yugoslav Republic of", "value": "MK" },
                { "text": "Madagascar", "value": "MG" },
                { "text": "Malawi", "value": "MW" },
                { "text": "Malaysia", "value": "MY" },
                { "text": "Maldives", "value": "MV" },
                { "text": "Mali", "value": "ML" },
                { "text": "Malta", "value": "MT" },
                { "text": "Marshall Islands", "value": "MH" },
                { "text": "Martinique", "value": "MQ" },
                { "text": "Mauritania", "value": "MR" },
                { "text": "Mauritius", "value": "MU" },
                { "text": "Mayotte", "value": "YT" },
                { "text": "Mexico", "value": "MX" },
                { "text": "Micronesia, Federated States of", "value": "FM" },
                { "text": "Moldova, Republic of", "value": "MD" },
                { "text": "Monaco", "value": "MC" },
                { "text": "Mongolia", "value": "MN" },
                { "text": "Montserrat", "value": "MS" },
                { "text": "Morocco", "value": "MA" },
                { "text": "Mozambique", "value": "MZ" },
                { "text": "Myanmar", "value": "MM" },
                { "text": "Namibia", "value": "NA" },
                { "text": "Nauru", "value": "NR" },
                { "text": "Nepal", "value": "NP" },
                { "text": "Netherlands", "value": "NL" },
                { "text": "Netherlands Antilles", "value": "AN" },
                { "text": "New Caledonia", "value": "NC" },
                { "text": "New Zealand", "value": "NZ" },
                { "text": "Nicaragua", "value": "NI" },
                { "text": "Niger", "value": "NE" },
                { "text": "Nigeria", "value": "NG" },
                { "text": "Niue", "value": "NU" },
                { "text": "Norfolk Island", "value": "NF" },
                { "text": "Northern Mariana Islands", "value": "MP" },
                { "text": "Norway", "value": "NO" },
                { "text": "Oman", "value": "OM" },
                { "text": "Pakistan", "value": "PK" },
                { "text": "Palau", "value": "PW" },
                { "text": "Palestinian Territory, Occupied", "value": "PS" },
                { "text": "Panama", "value": "PA" },
                { "text": "Papua New Guinea", "value": "PG" },
                { "text": "Paraguay", "value": "PY" },
                { "text": "Peru", "value": "PE" },
                { "text": "Philippines", "value": "PH" },
                { "text": "Pitcairn", "value": "PN" },
                { "text": "Poland", "value": "PL" },
                { "text": "Portugal", "value": "PT" },
                { "text": "Puerto Rico", "value": "PR" },
                { "text": "Qatar", "value": "QA" },
                { "text": "Reunion", "value": "RE" },
                { "text": "Romania", "value": "RO" },
                { "text": "Russian Federation", "value": "RU" },
                { "text": "RWANDA", "value": "RW" },
                { "text": "Saint Helena", "value": "SH" },
                { "text": "Saint Kitts and Nevis", "value": "KN" },
                { "text": "Saint Lucia", "value": "LC" },
                { "text": "Saint Pierre and Miquelon", "value": "PM" },
                { "text": "Saint Vincent and the Grenadines", "value": "VC" },
                { "text": "Samoa", "value": "WS" },
                { "text": "San Marino", "value": "SM" },
                { "text": "Sao Tome and Principe", "value": "ST" },
                { "text": "Saudi Arabia", "value": "SA" },
                { "text": "Senegal", "value": "SN" },
                { "text": "Serbia and Montenegro", "value": "CS" },
                { "text": "Seychelles", "value": "SC" },
                { "text": "Sierra Leone", "value": "SL" },
                { "text": "Singapore", "value": "SG" },
                { "text": "Slovakia", "value": "SK" },
                { "text": "Slovenia", "value": "SI" },
                { "text": "Solomon Islands", "value": "SB" },
                { "text": "Somalia", "value": "SO" },
                { "text": "South Africa", "value": "ZA" },
                { "text": "South Georgia and the South Sandwich Islands", "value": "GS" },
                { "text": "Spain", "value": "ES" },
                { "text": "Sri Lanka", "value": "LK" },
                { "text": "Sudan", "value": "SD" },
                { "text": "Suriname", "value": "SR" },
                { "text": "Svalbard and Jan Mayen", "value": "SJ" },
                { "text": "Swaziland", "value": "SZ" },
                { "text": "Sweden", "value": "SE" },
                { "text": "Switzerland", "value": "CH" },
                { "text": "Syrian Arab Republic", "value": "SY" },
                { "text": "Taiwan, Province of China", "value": "TW" },
                { "text": "Tajikistan", "value": "TJ" },
                { "text": "Tanzania, United Republic of", "value": "TZ" },
                { "text": "Thailand", "value": "TH" },
                { "text": "Timor-Leste", "value": "TL" },
                { "text": "Togo", "value": "TG" },
                { "text": "Tokelau", "value": "TK" },
                { "text": "Tonga", "value": "TO" },
                { "text": "Trinidad and Tobago", "value": "TT" },
                { "text": "Tunisia", "value": "TN" },
                { "text": "Turkey", "value": "TR" },
                { "text": "Turkmenistan", "value": "TM" },
                { "text": "Turks and Caicos Islands", "value": "TC" },
                { "text": "Tuvalu", "value": "TV" },
                { "text": "Uganda", "value": "UG" },
                { "text": "Ukraine", "value": "UA" },
                { "text": "United Arab Emirates", "value": "AE" },
                { "text": "United Kingdom", "value": "GB" },
                { "text": "United States", "value": "US" },
                { "text": "United States Minor Outlying Islands", "value": "UM" },
                { "text": "Uruguay", "value": "UY" },
                { "text": "Uzbekistan", "value": "UZ" },
                { "text": "Vanuatu", "value": "VU" },
                { "text": "Venezuela", "value": "VE" },
                { "text": "Viet Nam", "value": "VN" },
                { "text": "Virgin Islands, British", "value": "VG" },
                { "text": "Virgin Islands, U.S.", "value": "VI" },
                { "text": "Wallis and Futuna", "value": "WF" },
                { "text": "Western Sahara", "value": "EH" },
                { "text": "Yemen", "value": "YE" },
                { "text": "Zambia", "value": "ZM" },
                { "text": "Zimbabwe", "value": "ZW" }
            ];
            current.availableStates = ["Andhra Pradesh",
                "Arunachal Pradesh",
                "Assam",
                "Bihar",
                "Chhattisgarh",
                "Goa",
                "Gujarat",
                "Haryana",
                "Himachal Pradesh",
                "Jammu and Kashmir",
                "Jharkhand",
                "Karnataka",
                "Kerala",
                "Madhya Pradesh",
                "Maharashtra",
                "Manipur",
                "Meghalaya",
                "Mizoram",
                "Nagaland",
                "Odisha",
                "Punjab",
                "Rajasthan",
                "Sikkim",
                "Tamil Nadu",
                "Telangana",
                "Tripura",
                "Uttarakhand",
                "Uttar Pradesh",
                "West Bengal",
                "Andaman and Nicobar Islands",
                "Chandigarh",
                "Dadra and Nagar Haveli",
                "Daman and Diu",
                "Delhi",
                "Lakshadweep",
                "Puducherry"];
            current.EditingToggle= function () {
                current.isEditing(!this.isEditing());
            }
        };

        ko.applyBindings(memberViewModel);

        function addNewAddress(addresses) {
            console.log("New ", addresses);
            addresses.forEach((address) => {
                var obj = "{"
                    + "\'UserName\':'<%: Session["user"] %>',"
                    + "\'AddressType\':'"+address.addressType+"',"
                    + "\'AddressLine1\':'"+address.addressLine1+"',"
                    + "\'AddressLine2\':'"+address.addressLine2+"',"
                    + "\'City\':'"+address.city+"',"
                    + "\'PinCode\':'"+address.pinCode+"',"
                    + "\'Country\':'"+address.country+"',"
                    + "\'State\':'"+address.state+"',"
                    + "\'Phone\':'"+address.phone+"',"
                    + "\'Fax\':'"+address.fax+"'}";
                console.log(obj);
            
                $.ajax({
                    type: "POST",
                    url: "WebService.asmx/AddNewAddress",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: obj,
                    success: function (r) {
                        memberViewModel.isEditing(!memberViewModel.isEditing());
                        alert(r.d);
                    },
                    error: function (r) {
                        alert('error');
                        console.error(r.responseText);
                    },
                    failure: function (r) {
                        alert('failure');
                        console.log(r.responseText);
                    }
                });
                
                
            })

        }

        function editAddress(addresses) {
            console.log("New ", addresses);
            addresses.forEach((address) => {
                var obj = "{"
                    + "\'ID\':'"+address.id+"',"
                    + "\'UserName\':'<%: Session["user"] %>',"
                    + "\'AddressType\':'"+address.addressType+"',"
                    + "\'AddressLine1\':'"+address.addressLine1+"',"
                    + "\'AddressLine2\':'"+address.addressLine2+"',"
                    + "\'City\':'"+address.city+"',"
                    + "\'PinCode\':'"+address.pinCode+"',"
                    + "\'Country\':'"+address.country+"',"
                    + "\'State\':'"+address.state+"',"
                    + "\'Phone\':'"+address.phone+"',"
                    + "\'Fax\':'"+address.fax+"'}";
            
                $.ajax({
                    type: "POST",
                    url: "WebService.asmx/UpdateAddressByID",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: obj,
                    success: function (r) {
                        memberViewModel.EditingToggle();
                        console.log(r.d);
                    },
                    error: function (r) {
                        alert('error');
                        console.log(r.responseText);
                    },
                    failure: function (r) {
                        alert('failure');
                        console.log(r.responseText);
                    }
                });
                
                
            })

        }
        function deleteAddress(address) {
            $.ajax({
                type: "POST",
                url: "WebService.asmx/DeleteAddressByID",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                data: "{'ID':'" + address.id + "'}",
                success: function (r) {
                    memberViewModel.addresses.remove(address);
                    alert(r.d);
                },
                error: function (r) {
                    alert('error');
                    console.log(r.responseText);
                },
                failure: function (r) {
                    alert('failure');
                    console.log(r.responseText);
                }
            });

        }
        function setAddress() {
            $.ajax({
                type: "POST",
                url: "WebService.asmx/GetMemberAddress",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                data: "{'Username':'<%: Session["user"] %>'}",
                    success: function (r) {
                        var array = JSON.parse(r.d);
                        for (let i = 0; i < array.length; i++) {
                            var obj = {
                                id:             array[i]["ID"],
                                addressType:    ko.observable(array[i]["AddressType"]),
                                addressLine1:   ko.observable(array[i]["AddressLine1"]),
                                addressLine2:   ko.observable(array[i]["AddressLine2"]),
                                city:           ko.observable(array[i]["City"]),
                                pinCode:        ko.observable(array[i]["PinCode"]),
                                country:        ko.observable(array[i]["Country"]),
                                state:          ko.observable(array[i]["State"]),
                                phone:          ko.observable(array[i]["Phone"]),
                                fax:            ko.observable(array[i]["Fax"]),
                            }
                            memberViewModel.addresses.push(obj);
                        }
                    },
                    error: function (r) {
                        alert('error');
                        console.log(r.responseText);
                    },
                    failure: function (r) {
                        alert('failure');
                        console.log(r.responseText);
                    }
            });
        }

        function setDetails() {
            $.ajax({
                type: "POST",
                url: "WebService.asmx/GetMemberDetails",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                data: "{'Username':'<%: Session["user"] %>'}",
                success: function (r) {
                    var object = JSON.parse(r.d);
                    memberViewModel.firstName(object["Firstname"]);
                    memberViewModel.lastName(object["Lastname"]);
                    memberViewModel.email(object["Email"]);
                    memberViewModel.username(object["Username"]);
                    memberViewModel.gender(object["Gender"]);
                    memberViewModel.dob(object["Dob"]);
                    memberViewModel.specialisation(object["Specialisation"]);
                    memberViewModel.createdDate(object["CreateDate"]);
                },
                error: function (r) {
                    alert('error');
                    console.log(r.responseText);
                },
                failure: function (r) {
                    alert('failure');
                    console.log(r.responseText);
                }
            });
        }

    </script>
</asp:Content>

