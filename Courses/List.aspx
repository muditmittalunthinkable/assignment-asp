﻿<%@ Page Title="Courses"  Language="C#" AutoEventWireup="true"  MasterPageFile="~/Site.Master"  CodeFile="List.aspx.cs" Inherits="Account_CourseList"  Async="true" %>


<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="MainContent">
    <style>

        :root{ --main-color: #df5f16; }

        a.disabled {
            color: #ccc;
        }
        a.active{
            border: 1px solid grey;
        }

        .spinner-border{
            display: none;
            position: fixed;
            top: 50%;
            left: 50%;
            padding: 30px;
            z-index: 10;
        }

        .product-content .title{
            margin-top:40px;
            text-align:center;
            font-family:Roboto;
            font-size:12px;
            text-transform:uppercase;
        }

        .feature-title{
            font-weight:600;
            color:#000;
        }

        .feature-title h2{
            margin-top:60px;
            margin-bottom:50px;
            color: #000;
            font-family: Roboto;
            font-size: 38px;
            letter-spacing: 1px;
            text-transform: uppercase;
            font-weight: 700;
        }

        .price, .rating{
            display:inline-block;
            margin:0 auto;
            width:100%;
            text-align:center;
        }

        .product-grid{
            border:1px solid transparent;
            font-family: 'Roboto', sans-serif;
            padding:10px 10px;
            transition: all 0.5s;
            margin-bottom:80px;
            margin-top:20px;
        }

        .product-grid .product-image{
            position: relative;
            width: 15rem;
            height: 10rem;
            overflow: hidden;
        }
        .product-grid .product-image a.image{ display: block; }
        .product-grid .product-image img{
            width: 100%;
            height: auto;
          border-radius: 5px;
   
        }
        .product-grid .add-to-cart{
            color: #fff;
            background: #28a745;
            font-size: 34px;
            font-weight: 500;
            text-align: center;
            padding: 0 5px;
            display: block;
            position: absolute;
            bottom: -50px;
            right: 0;
            transition: all ease-in-out 0.35s;
            width:20%;
            opacity: 0;

        }
        .product-grid:hover .add-to-cart{ opacity:1; }
        .product-grid .add-to-cart:after{
            content: "\f101";
            color: #fff;
            font-family: 'Font Awesome 5 Free';
            font-weight: 900;
            display: inline-block;
            opacity: 0;
            transition: all 0.5s;
    
        }
        .product-grid .add-to-cart:hover:after{
            padding-left: 10px;
            opacity: 1;
        }
        .product-grid .product-content{ position: relative; }
        .product-grid .product-stock{
            color: #999;
            font-size: 16px;
            font-weight: 600;
            line-height: 35px;
            border-bottom: 1px solid #c1c1c1;
            display: block;
            margin: 0 0 15px;
        }
        .product-grid .icon{
            padding: 0;
            margin: 0;
            list-style: none;
            opacity: 0;
            position: absolute;
            top: -15px;
            right: 0;
            transition: all ease-in-out 0.35s;
        }
        .product-grid:hover .icon{
            opacity: 1;
            top: 8px;
        }
        .product-grid .icon li{ display: inline-block; }
        .product-grid .icon li a{
            color: #222;
            font-size: 16px;
            margin: 0 4px;
        }
        .product-grid .icon li a:hover{ color: var(--main-color); }
        .product-grid .title{
             font-family:Roboto;
            font-size:15px;
            text-transform:uppercase;
            font-weight:700;
            line-height:2px;
        }
        .product-grid .title a{
            color: #222;
            transition: all 0.5s ease-out 0s;
        }
        .product-grid .title a:hover{ color: var(--main-color); }
        .product-grid .category{
            font-size: 18px;
            margin: 0 0 3px;
            display: block;
        }
        .product-grid .category a{
            color: #222;
            transition: all 0.3s ease 0s;
        }
        .product-grid .category a:hover{ color: var(--main-color); }
        .product-grid .price{
            font-family:Roboto;
            font-size:14px;
            font-weight:700;
            line-height:0px;
            display: inline-block;
            color:#000;
        }
        .product-grid .rating{
            padding: 0;
            margin: 0;
            list-style: none;
            float: right;
        }
        .product-grid .rating li{
            color: #FFB14B;
            font-size: 1rem;
            display: inline-block;
        }

        .product-grid .rating li.disable{ color: #c1c1c1; }
        @media only screen and (max-width:990px){
            .product-grid{ margin-bottom: 30px; }
        }
    </style>

    <div class="container">
        <div class="spinner-border text-primary" role="status">
          <span class="sr-only">Loading...</span>
        </div>
        <div class="col-md-12 text-center feature-title">
            <h2>FEATURED COURSES</h2>
        </div>

        <!-- Pagination -->
        <div class="col-12">

            Records on 1 page: 
                <span id="recordsOnOnePage">
                    <select onchange="setPagination()">
                        <option>20</option>
                        <option>25</option>
                        <option>30</option>
                        <option>35</option>
                        <option>40</option>
                        <option>45</option>
                        <option>50</option>
                    </select>
                </span>
            
            &nbsp;&nbsp;
            
            Total Records : <span id="totalRecords"></span>
            &nbsp;&nbsp;
            
            Total Pages: <span id="totalPages"></span>
            &nbsp;&nbsp;
            
            Current Page: <span id="currentPage"></span>
            &nbsp;&nbsp;
            
            Records from: <span id="recordFrom"></span> to: <span id="recordTo"></span>
            </div>

            <div class="col-12 text-center">
                <a class="btn" id="prevPagination"><</a>
                <span id="pagination"></span>
                <a class="btn" id="nextPagination">></a>
            </div>
        <!-- /Pagination -->

        <div class="row" data-bind="foreach: { data: courses, as: 'course' }">
            <div class="col-3">
                <div class="product-grid">
                    <div class="product-image">
                        <img class="pic-1" data-bind="attr: { src: course.src }">
                        <button type="button" class="btn add-to-cart" data-bind="click: $parent.clickCart"> 
                            <i class="fas fa-cart-plus"></i>
                        </button>
                    </div>
                    <div class="product-content mb-3">
                   
                        <h3 class="title"><a data-bind="attr: { href: 'Details?courseId='+course.id }, text: course.title">Product Name</a></h3>
                        <div class="price" data-bind="text: 'by- '+course.instructor()">$21.60</div>
                    
                        <div class="price" data-bind="text: '₹'+course.price()">$21.60</div>
                        <ul class="rating my-2">
                           <li class="fas fa-star" data-bind="text: course.rating()"></li>
                        </ul>
                    </div>

                    <div class="action-buttons text-center mt-4">
                        <!-- ko if: course.inCart() == 1 -->
                            <button type="button" class="btn btn-outline-success disabled" >IN CART</button>
                        <!-- /ko -->
                        <!-- ko if: course.inPurchase == 1 -->
                            <button type="button" class="btn btn-success" >PURCHASED</button>
                        <!-- /ko -->
                        <!-- ko if: course.inCart() == 0 -->
                            <!-- ko if: course.inPurchase == 0 -->
                                <button type="button" class="btn btn-outline-dark" data-bind="click: $parent.clickCart">ADD TO CART</button>
                            <!-- /ko -->
                        <!-- /ko -->
                    </div>
                </div>
            </div>

        </div>
    </div>           


    <script>
        setPagination();
        gotoPage(1, $("#recordsOnOnePage option:selected").text());
        var totalPages;

        async function setPagination() {
            var recordsOnOnePage = $("#recordsOnOnePage option:selected").text();
            var totalRecords;
            var pages = "";
                await $.ajax({
                    type: "POST",
                    url: "../CoursesService.asmx/CountAllCourses",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (r) {
                        //alert(r.d);
                        totalRecords = r.d;
                        $("#totalRecords").text(totalRecords);
                        totalPages = parseInt(totalRecords / recordsOnOnePage);
                        if (parseInt(totalRecords / recordsOnOnePage) < (totalRecords / recordsOnOnePage)) {
                            totalPages += 1;
                        }
                        $("#totalPages").text(totalPages);
                    },
                    error: function (r) {
                        alert('error');
                        console.log(r.responseText);
                    },
                    failure: function (r) {
                        alert('failure');
                        console.log(r.responseText);
                    }
                });

            for (let i = 1; i <= totalPages; i++) {
                pages += `<a href="#" class="btn" onclick="gotoPage(${i}, ${recordsOnOnePage}, ${totalRecords})">${i}</a>`;
            }
            $("#pagination").html(pages);
            gotoPage(1, recordsOnOnePage, totalRecords);
        }

        $("#prevPagination").click(function () {
            gotoPage(parseInt($("#currentPage").text()) - 1, $("#recordsOnOnePage option:selected").text());
        })
        $("#nextPagination").click(function () {
            gotoPage(parseInt($("#currentPage").text()) + 1, $("#recordsOnOnePage option:selected").text());
        })

        function gotoPage(pageNum, recordsOnOnePage, totalRecords = 100) {
            const Username = '<%:Session["user"]%>';

            var from = ((pageNum - 1) * recordsOnOnePage) + 1;
            var to = (((pageNum - 1) * recordsOnOnePage) + recordsOnOnePage);
            to = to > totalRecords ? totalRecords : to;

            $("#currentPage").text(pageNum);
            $("#recordFrom").text(from);
            $("#recordTo").text(to);
            $(`span#pagination`).children().removeClass("active");
            $(`span#pagination a:nth-child(${pageNum})`).addClass("active");

            if (pageNum == 1)
                $("#prevPagination").addClass("disabled");
            else
                $("#prevPagination").removeClass("disabled");


            if (pageNum == totalPages)
                $("#nextPagination").addClass("disabled");
            else
                $("#nextPagination").removeClass("disabled");



            $.ajax({
                type: "POST",
                url: "../CoursesService.asmx/GetAllCourses",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                data: "{'Username': '"+Username+"', 'recordFrom': '" + from + "', 'recordTo':'" + to + "'}",
                success: function (r) {
                    var array = JSON.parse(r.d);
                    memberViewModel.courses.removeAll();
                    for (let i = 0; i < array.length; i++) {
                        var obj = {
                            id:             array[i]["ID"],
                            title:    ko.observable(array[i]["Title"]),
                            instructor:   ko.observable(array[i]["Instructor"]),
                            price:   ko.observable(array[i]["Price"]),
                            rating: ko.observable(array[i]["Rating"]),
                            productId: ko.observable(array[i]["ProductID"]),
                            inCart: ko.observable(array[i]["InCart"]),
                            inPurchase: (array[i]["InPurchase"]),
                            src: '../assets/pic' + i % 3 + '.jpg'
                        }
                        memberViewModel.courses.push(obj);
                    }

                },
                error: function (r) {
                    alert('error');
                    console.log(r.responseText);
                },
                failure: function (r) {
                    alert('failure');
                    console.log(r.responseText);
                }
            });

        }


    </script>
    <script>

        var memberViewModel = new function () {
            current = this;
            current.clickCart = function (course) {
                addToCart('<%: Session["user"] %>', course.id, course.title(), course.productId(),
                    course.price());
                course.inCart(1);
            }
            current.gotoDetails = function (course) {
                console.log(course);
            }
            current.courses = ko.observableArray();
        }
        ko.applyBindings(memberViewModel);
//        setCourses();
        function setCourses() {
            const Username = '<%: Session["user"] %>';
            $.ajax({
                type: "POST",
                url: "../CoursesService.asmx/GetAllCourses",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                data: "{'Username':'"+Username+"'}",
                success: function (r) {
                    var array = JSON.parse(r.d);
                    for (let i = 0; i < array.length; i++) {
                        var obj = {
                            id:             array[i]["ID"],
                            title:    ko.observable(array[i]["Title"]),
                            instructor:   ko.observable(array[i]["Instructor"]),
                            price:   ko.observable(array[i]["Price"]),
                            rating: ko.observable(array[i]["Rating"]),
                            productId: ko.observable(array[i]["ProductID"]),
                            inCart: ko.observable(array[i]["InCart"]),
                            inPurchase: (array[i]["InPurchase"]),
                            src: '../assets/pic' + i % 3 + '.jpg'
                        }
                        memberViewModel.courses.push(obj);
                    }
                },
                error: function (r) {
                    alert('error');
                    console.log(r.responseText);
                },
                failure: function (r) {
                    alert('failure');
                    console.log(r.responseText);
                }
            });
        }

        function addToCart(username, courseId, title, productId, price) {
            if (username != '') {
                $(".spinner-border").show();
                $.ajax({
                    type: "POST",
                    url: "../CoursesService.asmx/AddToCart",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: "{'Username':'" + username + "','CourseID':'" + courseId + "','ProductTitle':'" + title + "','ProductID':'" + productId + "','Price':'" + price + "'}",
                    success: function (r) {
                        $(".spinner-border").hide();
                        //alert(r.d);
                    },
                    error: function (r) {
                        $(".spinner-border").hide();
                        alert('error');
                        console.log(r.responseText);
                    },
                    failure: function (r) {
                        $(".spinner-border").hide();
                        alert('failure');
                        console.log(r.responseText);
                    }
                });
            }
            else {
                window.location.href ="/Account/Login" ;
            }
        }
    </script>
</asp:Content>