﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Site.Master" CodeFile="Billing.aspx.cs" Inherits="Courses_Billing" %>

<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="MainContent">
    <style>
        .error {
          color: red;
          margin-left: 5px;
        }
        .spinner-border{
            display: none;
            position: fixed;
            top: 50%;
            left: 50%;
            padding: 30px;
            z-index: 10;
        }

    </style>
    <div class="p-5">
        <div class="spinner-border text-primary" role="status">
          <span class="sr-only">Loading...</span>
        </div>

        <h4>Add Billing Details.</h4>
        <hr />
        <div class="row">
            <div class="col-7">
                <div class="form-group">
                    <label for="username" class="col-form-label">Username</label>
                    <input type="text" disabled class="form-control" data-bind="value: username()">
                </div>
                <div class="form-group">
                    <label for="username" class="col-form-label">Full Name</label>
                    <input type="text" disabled class="form-control" data-bind="value: fullName()">
                </div>
                <div class="form-group">
                    <label for="username" class="col-form-label">Mobile Number</label>
                    <input type="text" class="form-control" id="mobileNum" data-bind="textInput: mobile">
                </div>
                <div class="form-group">
                    <label for="username" class="col-form-label">Email</label>
                    <input type="text" disabled class="form-control" data-bind="value: email()">
                </div>
                <button type="button" class="btn btn-primary" data-bind="click: proceedBtn">Proceed to Pay</button>
            </div>
            <div class="col-5 border-left border-top">
                
                <div><label class="col-form-label">Number of Products: <span class="font-weight-bold" data-bind="text: numOfProducts()"></span></label></div>
                <div><label class="col-form-label">SubTotal: <span class="font-weight-bold" data-bind="text: '₹'+subtotal()"></span></label></div>
                <div><label class="col-form-label">Tax: <span class="font-weight-bold" data-bind="text: '₹'+tax()"></span></label></div>
                <div><label class="col-form-label">Total Price: <span class="font-weight-bold" data-bind="text: '₹'+totalPrice()"></span></label></div>
                <div>
                    Products:
                    <ul class="list-unstyled" data-bind="foreach: courses()">
                        <li data-bind="text: $index()+1 +'. '+productTitle"></li>
                    </ul>
                </div>
            </div>
        </div>


    </div>
    <script>
        var memberViewModel = new function () {
            current = this;
            current.courses = ko.observableArray();//
            current.username = ko.observable('');
            current.fullName =  ko.observable('');
            current.mobile =    ko.observable('');
            current.email = ko.observable('');

            current.numOfProducts = ko.observable('');//
            current.subtotal = ko.computed(function () {
                courses = ko.toJS(current.courses);
                let total = 0;
                courses.forEach(function ({ price }) {
                    total+= (Number(price));
                });
                return total;
            });
            current.tax = ko.computed(function () {
                    return (current.subtotal() * 7)/100;
            });
            current.totalPrice = ko.computed(function () {
                return (current.subtotal() + current.tax());
            });//
            current.proceedBtn = function () {
                if (current.mobile() == '') {
                    alert("Enter Mobile number");
                    return;
                }
                console.log(current.mobile());

                BillDate = new Date().toISOString().substring(0, 10);
                CustomerUsername = current.username();
                CustomerName = current.fullName();
                Subtotal = current.subtotal();
                Tax = current.tax();
                TotalPrice = current.totalPrice();
                NumberOfProducts = current.numOfProducts();
                MobileNumber = current.mobile();
                EmailID = current.email();
                $(".spinner-border").show();
                addBilling(BillDate, CustomerUsername, CustomerName, Subtotal, Tax, TotalPrice, NumberOfProducts, MobileNumber, EmailID);
                setTimeout(() => {
                    window.location.replace("/Courses/My-Courses");
                },4000)
            }
        }
        ko.applyBindings(memberViewModel);

    </script>
    <script>
        setCart();
        setUserDetails();
        function addBilling(BillDate, CustomerUsername, CustomerName, Subtotal, Tax, TotalPrice, NumberOfProducts, MobileNumber, EmailID) {
            billData = "{'BillDate': '" + BillDate + "','CustomerUsername': '" + CustomerUsername + "','CustomerName': '" + CustomerName + "','Subtotal': '" + Subtotal + "','Tax': '" + Tax + "','TotalPrice': '" + TotalPrice + "','NumberOfProducts': '" + NumberOfProducts + "','MobileNumber': '" + MobileNumber + "','EmailID': '" + EmailID + "'}";
            //console.log(billData);
            $.ajax({
                type: "POST",
                url: "../CoursesService.asmx/AddToBill",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                data: billData,
                success: function (r) {
                    console.log(r.d);
                    addPurchase(CustomerUsername, BillDate, r.d);
                },
                error: function (r) {
                    alert('error');
                    console.log(r.responseText);
                },
                failure: function (r) {
                    alert('failure');
                    console.log(r.responseText);
                }
            });
        }
        function addPurchase(Username, PurchaseDate, BillID) {
            courses = ko.toJS(memberViewModel.courses);

            courses.forEach((course) => {
                Data = "{'Username': '" + Username + "','CourseID': '" + course.courseId + "','ProductTitle': '" + course.productTitle + "','ProductID': '" + course.productId + "','Price': '" + course.price + "','PurchaseDate': '" + PurchaseDate + "','BillID': '" + BillID + "'}";

                console.log(Data);
                $.ajax({
                    type: "POST",
                    url: "../CoursesService.asmx/AddToPurchase",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: Data,
                    success: function (r) {
                        console.log("add pur",r.d);
                        removeFromCart(course.id);
                    },
                    error: function (r) {
                        alert('error');
                        console.log(r.responseText);
                    },
                    failure: function (r) {
                        alert('failure');
                        console.log(r.responseText);
                    }
                });
            });
        }
        function removeFromCart(id) {
            $.ajax({
                type: "POST",
                url: "../CoursesService.asmx/DeleteFromCartByID",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                data: "{'ID':'"+id+"'}",
                success: function (r) {
                    console.log("rem cart", r.d);
                },
                error: function (r) {
                    alert('error');
                    console.error(r.responseText);
                },
                failure: function (r) {
                    alert('failure');
                    console.failure(r.responseText);
                }
            });
        }
        function setUserDetails() {
            const Username = '<%:Session["user"]%>';
            if(Username!='')
                $.ajax({
                    type: "POST",
                    url: "../WebService.asmx/ReadByUsername",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: "{'Username':'"+Username+"'}",
                    success: function (r) {
                        var obj = JSON.parse(r.d);
                        memberViewModel.username(obj["Username"]);
                        memberViewModel.fullName(obj["Firstname"] + ' ' + obj["Lastname"] );
                        memberViewModel.email(obj["Email"]);
                    },
                    error: function (r) {
                        alert('error');
                        console.error(r.responseText);
                    },
                    failure: function (r) {
                        alert('failure');
                        console.failure(r.responseText);
                    }
                });

        }
        function setCart() {
            const Username = '<%:Session["user"]%>';
            if(Username!='')
                $.ajax({
                    type: "POST",
                    url: "../CoursesService.asmx/GetCartProducts",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: "{'Username':'"+Username+"'}",
                    success: function (r) {
                        var array = JSON.parse(r.d);
                        for (let i = 0; i < array.length; i++) {
                            var obj = {
                                id:             array[i]["ID"],
                                courseId:             array[i]["CourseID"],
                                productTitle:    (array[i]["ProductTitle"]),
                                productId: (array[i]["ProductID"]),
                                price:   (array[i]["Price"]),
                            }
                            memberViewModel.courses.push(obj);
                        }
                        memberViewModel.numOfProducts(array.length);
                    },
                    error: function (r) {
                        alert('error');
                        console.error(r.responseText);
                    },
                    failure: function (r) {
                        alert('failure');
                        console.failure(r.responseText);
                    }
                });
        }
    </script>

</asp:Content>