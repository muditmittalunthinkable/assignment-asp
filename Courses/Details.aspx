﻿<%@ Page Title="Details" Language="C#" AutoEventWireup="true" MasterPageFile="~/Site.Master" CodeFile="Details.aspx.cs" Inherits="Courses_Details" %>

<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="MainContent">
    <style>
        .container {
             max-width: 960px;
             font-size: 20px;
        }
        .spinner-border{
            display: none;
            position: fixed;
            top: 50%;
            left: 50%;
            padding: 30px;
            z-index: 10;
        }

        *, *:before, *:after {
             box-sizing: border-box;
        }
        .cart-Btn{
            font-size: 1.5rem;
        }
        .product__title {
             text-transform: uppercase;
             font-weight: 400;
             font-size: 2em;
             margin: 20px 0px 0px;
        }
        .product__byline {
             color: #998f52;
             text-transform: uppercase;
             letter-spacing: .05em;
             font-size: 1.2em;
        }
        .product__byline em {
             margin-right: 3px;
             text-transform: lowercase;
             letter-spacing: .0em;
             font-size: .8em;
        }
        .product__image {
             width: 40rem;
             margin: 20px 0px;
        }
        @media only screen and (min-width: 768px) {
             .product__notify {
                 display: none;
            }
        }
        .price-option {
             background: #f6f5ea;
             border-radius: 5px;
             padding: 10px 10px 6px;
             margin-bottom: 20px;
        }
        .price-option__icon {
             margin: 4px 10px 4px 2px;
             float: left;
             font-size: 1.9em;
             color: #989581;
        }
        .price-option__title {
             padding: 0;
             font-size: .9em;
             margin: 0px 0px -8px;
             text-transform: uppercase;
        }
        .price-option__price {
             padding: 0;
             margin: 0;
             font-size: .75em;
             color: #998f52;
        }
        .price-option__price.rating li{
            margin-top: 10px;
            color: #FFB14B;
            display: inline-block;
        }

        .price-option__button {
             float: right;
             text-transform: uppercase;
             font-weight: 400;
             font-size: .75em;
             padding: 5px 8px;
             margin: -12px 3px 0px 0px;
             background: linear-gradient(#7db72f,#4e7d0e);
             border: solid 1px #64991E;
        }
        .price-option__button:hover {
             background: #4e7d0e;
             border: solid 1px #64991E;
        }
        @media only screen and (max-width: 768px) {
             .price-option__button {
                 font-size: 1.1em;
                 height: 61px;
                 width: 55px;
                 margin-right: -10px;
                 margin-top: -27px;
                 border-top-left-radius: 0px;
                 border-bottom-left-radius: 0px;
            }
        }
        .details__container {
             margin: 10px 0px;
             border-top: 1px solid #CCC;
        }
        .details__title {
             margin-top: 27px;
             color: #997e11;
             text-transform: uppercase;
             font-weight: 500;
             font-size: 1em;
             letter-spacing: .1em;
        }
        .details__title span {
             letter-spacing: 0px;
             text-transform: lowercase;
             font-family: 'Droid Serif', serif;
             font-size: .7em;
             font-weight: 400;
             font-style: italic;
             margin-right: .5em;
        }
        .details__text {
             font-family: 'Droid Serif', serif;
             font-size: .9em;
             line-height: 1.7em;
        }
        .details__list-item {
             font-family: 'Open Sans', sans-serif;
             font-size: 0.7em;
             line-height: 2.2em;
        }

 
    </style>


    <div class="container">
        <div class="spinner-border text-primary" role="status">
          <span class="sr-only">Loading...</span>
        </div>

        <div class="row product">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <h2 class="product__title float-left" data-bind="text: course().title"></h2>
                <!-- ko if: inCart() == 1 -->
                    <button type="button" class="cart-Btn float-right mt-4 btn btn-outline-success disabled" >IN CART</button>
                <!-- /ko -->
                <!-- ko if: course().inPurchase == 1 -->
                    <button type="button" class="cart-Btn float-right mt-4 btn btn-success" >PURCHASED</button>
                <!-- /ko -->
                <!-- ko if: inCart() == 0 -->
                    <!-- ko if: course().inPurchase == 0 -->
                        <button type="button" class="cart-Btn float-right mt-4 btn btn-outline-success" data-bind="click: clickCart">
                            <i class="fas fa-cart-plus"></i>
                        </button>                        
                    <!-- /ko -->
                <!-- /ko -->

            </div>
            <div class="col-md-12">
                <span class="product__byline" data-bind="html: '<em>by </em>'+course().instructor"></span>
                <img class="product__image" data-bind="attr: { src: course().imgSrc }"/>
            </div>
        </div>
        <div class="row price">
            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="price-option"><i class="price-option__icon fa fa-book"></i>
                    <p class="price-option__title">Cost</p>
                    <span class="price-option__price" data-bind="text: course().price"></span>
                </div>
            </div>
            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="price-option"><i class="price-option__icon fa fa-book"></i>
                    <p class="price-option__title">Rating</p>
                    <ul class="rating price-option__price" data-bind="foreach: ratingArray()">
                        <li class="fas fa-star"></li>
                    </ul>
                </div>
            </div>
            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="price-option"><i class="price-option__icon fa fa-closed-captioning"></i>
                    <p class="price-option__title">Language</p>
                    <span class="price-option__price" data-bind="text: course().language"></span>
                </div>
            </div>
            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="price-option"><i class="price-option__icon fa fa-book"></i>
                    <p class="price-option__title">Category</p>
                    <span class="price-option__price" data-bind="text: course().category"></span>
                </div>
            </div>

            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="price-option"><i class="price-option__icon fa fa-newspaper"></i>
                    <p class="price-option__title">Length</p>
                    <span class="price-option__price" data-bind="text: course().length"></span>
                </div>
            </div>
        </div>
        <div class="row details">
            <div class="col-md-8 col-sm-8 col-xs-12">
                <div class="details__container">
                    <h3 class="details__title">What you'll learn</h3>
                    <p class="details__text" data-bind="html: course().perks"></p>
                    <hr />
                    <h3 class="details__title">Description</h3>
                    <p class="details__text" data-bind="html: course().description"></p>
                    <hr />
                    <h3 class="details__title">Pre-Requisites</h3>
                    <p class="details__text" data-bind="html: course().requirements"></p>
                </div>
            </div>
            <!--
            <div class="col-md-4 col-sm-4 col-xs-12">
                <div class="details__container">
                    <h3 class="details__title">Contents</h3>
                    <li class="details__list-item">Our Responsive Web</li>
                    <li class="details__list-item">The Flexible Grid</li>
                    <li class="details__list-item">Flexible Images </li>
                    <li class="details__list-item">Media Queries</li>
                    <li class="details__list-item">Becoming Responsive</li>
                </div>
            </div>
                -->
            <div class="col-md-4 col-sm-4 col-xs-12">
                <div class="details__container">
                    <h3 class="details__title">Additional Info</h3>
                    <ul class="list-unstyled">
                        <li class="details__list-item"><i class="fas fa-caret-right"></i> Downloadable Resources</li>
                        <li class="details__list-item"><i class="fas fa-caret-right"></i> High Quality Content</li>
                        <li class="details__list-item"><i class="fas fa-caret-right"></i> Certificate on Completion</li>
                    </ul>
                </div>
            </div>
            <div class="col-12 text-center m-5">
                <!-- ko if: inCart() == 1 -->
                    <button type="button" class="btn btn-outline-success disabled cart-Btn" >IN CART</button>
                <!-- /ko -->
                <!-- ko if: course().inPurchase == 1 -->
                    <button type="button" class="btn btn-success cart-Btn" >PURCHASED</button>
                <!-- /ko -->
                <!-- ko if: inCart() == 0 -->
                    <!-- ko if: course().inPurchase == 0 -->
                        <button type="button" data-bind="click: clickCart" class="cart-Btn btn-outline-success btn"><i class="fas fa-shopping-cart mr-3"></i>ADD TO CART</button>
                    <!-- /ko -->
                <!-- /ko -->
            </div>
        </div>
    </div>


    <script>
        function getUrlVars()
        {
            var vars = [], hash;
            var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
            for(var i = 0; i < hashes.length; i++)
            {
                hash = hashes[i].split('=');
                vars.push(hash[0]);
                vars[hash[0]] = hash[1];
            }
            return vars;
        }
        var urlVals = getUrlVars();
        var courseId = urlVals.courseId;
        var memberViewModel = new function () {
            current = this;
            current.clickCart = function () {
                addToCart('<%: Session["user"] %>', current.course().id, current.course().title, current.course().productId,
                    current.course().price);
                this.inCart(1);
            }
            current.inCart = ko.observable('0');
            current.ratingArray = ko.observableArray();
            current.course = ko.observable({
                id: '',
                title: '',
                price: '',
                rating: '',
                instructor: '',
                category: '',
                language: '',
                requirements: ``,
                description: ``,
                length: '',
                perks: ``,
                productId: '',
                imgSrc: '',
                inCart: ko.observable(''),
                inPurchase: ''
            });
        }
        ko.applyBindings(memberViewModel);
        setDetails();
        function setDetails() {
//            console.log(courseId);
            const Username = '<%:Session["user"]%>';
            $.ajax({
                type: "POST",
                url: "../CoursesService.asmx/GetCourseByID",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                data: "{'ID': '"+courseId+"', 'Username':'"+Username+"'}",
                success: function (r) {
//                    console.log(r.d);
                    var array = JSON.parse(r.d);
//                    console.log(array);
                    var obj = {
                        id:             array["ID"],
                        title:    array["Title"],
                        price:   array["Price"],
                        rating: array["Rating"],
                        instructor:   array["Instructor"],
                        category: array["Category"],
                        language: array["Language"],
                        requirements: array["Requirements"],
                        description: array["Description"],
                        length: array["Length"],
                        perks: array["Perks"],
                        productId: array["ProductID"],
                        inPurchase: array["InPurchase"],
                        imgSrc: '../assets/pic0.jpg'
                    }
                    memberViewModel.inCart(array["InCart"]);
                    console.log(obj);
                    memberViewModel.course(obj);
                    for (var i = 0; i < Number(obj.rating); i++) {
                        memberViewModel.ratingArray.push(0);
                    }
                },
                error: function (r) {
                    alert('error');
                    console.log(r.responseText);
                },
                failure: function (r) {
                    alert('failure');
                    console.log(r.responseText);
                }
            });

        }

        function addToCart(username, courseId, title, productId, price) {
            //            console.log(username, courseId, title, productId, price)
            if (username != '') {
                $(".spinner-border").show();
                $.ajax({
                    type: "POST",
                    url: "../CoursesService.asmx/AddToCart",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: "{'Username':'" + username + "','CourseID':'" + courseId + "','ProductTitle':'" + title + "','ProductID':'" + productId + "','Price':'" + price + "'}",
                    success: function (r) {
                        $(".spinner-border").hide();
                        //setDetails();
                        alert(r.d);
                    },
                    error: function (r) {
                        $(".spinner-border").hide();
                        alert('error');
                        console.log(r.responseText);
                    },
                    failure: function (r) {
                        $(".spinner-border").hide();
                        alert('failure');
                        console.log(r.responseText);
                    }
                });
            }
            else {
                window.location.href = "/Account/Login";
            }
        }

    </script>
</asp:Content>