﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Courses_Details : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["user"] == null)
        {
            Response.Redirect("/Account/Login?ReturnUrl=%2FCourses%2FDetails%3FcourseId%3D"+ Request.QueryString["courseId"]);
        }

    }
}