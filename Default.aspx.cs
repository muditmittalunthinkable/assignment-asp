﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _Default : Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["user"] == null)
        {
            Response.Redirect("/Account/Login");
        }
        else { 
//            if (!Page.IsPostBack)
//              BindTableData();

            if (Roles.GetRolesForUser(Session["user"].ToString()).Contains("Administrator"))
            {
                MemberPanel.Visible = false;
                AdminPanel.Visible = true;
            }
            else
            {
                AdminPanel.Visible = false;
                MemberPanel.Visible = true;

            }

        }
    }
//    SqlDataAdapter da;
//   DataSet ds = new DataSet();
//    StringBuilder htmlTable = new StringBuilder();

//    private void BindTableData()
//    {
//        SqlConnection con = new SqlConnection();
//        con = new SqlConnection(ConfigurationManager.ConnectionStrings["dbconnection"].ConnectionString);
//
//        SqlCommand cmd = new SqlCommand("SELECT * FROM [dbo].[UserAccount]", con);
//        da = new SqlDataAdapter(cmd);
//        da.Fill(ds);
//        con.Open();
//        cmd.ExecuteNonQuery();
//        con.Close();
//
//        htmlTable.Append("<tbody>");
//
//        if (!object.Equals(ds.Tables[0], null))
//        {
//            if (ds.Tables[0].Rows.Count > 0)
//            {
//                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
//                {
//                    htmlTable.Append("<tr>");
//                    htmlTable.Append("<td>" + ds.Tables[0].Rows[i]["Firstname"] + "</td>");
//                    htmlTable.Append("<td>" + ds.Tables[0].Rows[i]["Lastname"] + "</td>");
//                    htmlTable.Append("<td>" + ds.Tables[0].Rows[i]["Email"] + "</td>");
//                    htmlTable.Append("<td>" + ds.Tables[0].Rows[i]["Username"] + "</td>");
//                    htmlTable.Append("<td>" + ds.Tables[0].Rows[i]["Gender"] + "</td>");
//                    htmlTable.Append("<td>" + ds.Tables[0].Rows[i]["Dob"] + "</td>");
//                    htmlTable.Append("<td>" + ds.Tables[0].Rows[i]["Specialisation"] + "</td>");
//                    htmlTable.Append("<td>" + ds.Tables[0].Rows[i]["CreatedBy"] + "</td>");
//                    htmlTable.Append(
//                        "<td>" +
//                        "<p id='edit_btn' onclick='edit_click(" + ds.Tables[0].Rows[i]["ID"] + ")' class='btn btn-primary'>EDIT</p>" +
//                        "</td>");
//                    htmlTable.Append(
//                        "<td>" +
//                        "<p id='delete_btn' onclick='delete_click(" + ds.Tables[0].Rows[i]["Username"] + ")' class='btn btn-danger'>DELETE</p>" +
//                        "</td>");
//                    htmlTable.Append("</tr>");
//                }
//                htmlTable.Append("</tbody>");
//                DBDataPlaceHolder.Controls.Add(new Literal { Text = htmlTable.ToString() });
//            }
//            else
//            {
//                htmlTable.Append("<tr>");
//                htmlTable.Append("<td align='center' colspan='4'>There is no Record.</td>");
//                htmlTable.Append("</tr>");
//            }
//        }
//    }

}