﻿<%@ Application Language="C#" %>
<%@ Import Namespace="Assignment" %>
<%@ Import Namespace="System.Web.Optimization" %>
<%@ Import Namespace="System" %>
<%@ Import Namespace="System.Collections.Generic" %>
<%@ Import Namespace="System.Linq" %>
<%@ Import Namespace="System.Web" %>
<%@ Import Namespace="System.Web.Routing" %>
<%@ Import Namespace="System.Web.Security" %>
<%@ Import Namespace="System.Web.SessionState" %>
<%@ Import Namespace="System.Web.Http" %>
<script runat="server">

    void Application_Start(object sender, EventArgs e)
    {
        RouteConfig.RegisterRoutes(RouteTable.Routes);
        BundleConfig.RegisterBundles(BundleTable.Bundles);
        RouteTable.Routes.MapHttpRoute(  
            name: "WebApi",  
            routeTemplate: "api/{controller}/{id}",
            defaults: new {  
                id = System.Web.Http.RouteParameter.Optional  
            }  
        );
    }

</script>
