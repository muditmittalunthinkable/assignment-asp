﻿using Microsoft.AspNet.Identity;
using System;
using System.Linq;
using System.Web.UI;
using System.Web.Security;
using Assignment;
using System.Drawing;
using System.Data.SqlClient;
using System.Configuration;

public partial class Account_Register : Page
{
    SqlConnection myConnection = new 
        SqlConnection(ConfigurationManager.ConnectionStrings["dbconnection"].ConnectionString);

    protected void Page_Load(object sender, EventArgs e)
    {
    }

    protected void CreateUser_Click(object sender, EventArgs e)
    {
        MembershipCreateStatus createStatus;
        MembershipUser user = Membership.CreateUser(
            UserName.Text, Password.Text, Email.Text,
            Question.Text, Answer.Text, true, out createStatus);
        
        switch (createStatus)
        {
            //This Case Occured whenver User Created Successfully in database  
            case MembershipCreateStatus.Success:
                myConnection.Open();
                string query;
                if (Session["user"]==null)
                query = "Insert into [dbo].[UserAccount] " +
                    "(Firstname,Lastname,Email,Username,Gender,Dob,Specialisation) " +
                    "Values (@firstname,@lastname,@Email,@username,@gender,@dob,@specialisation)";
                else
                    query = "Insert into [dbo].[UserAccount] " +
                        "(Firstname,Lastname,Email,Username,Gender,Dob,Specialisation, CreatedBy) " +
                        "Values (@firstname,@lastname,@Email,@username,@gender,@dob,@specialisation,@createdBy)";


                SqlCommand insertCommand = new SqlCommand(query, myConnection);
                insertCommand.Parameters.AddWithValue("@firstname", FirstName.Text);
                insertCommand.Parameters.AddWithValue("@lastname", LastName.Text);
                insertCommand.Parameters.AddWithValue("@Email", Email.Text);
                insertCommand.Parameters.AddWithValue("@username", UserName.Text );
                insertCommand.Parameters.AddWithValue("@gender", Gender.Text);
                insertCommand.Parameters.AddWithValue("@dob", DOB.Text);
                insertCommand.Parameters.AddWithValue("@specialisation", Specialisation.Text);
                if (Session["user"] != null)
                    insertCommand.Parameters.AddWithValue("@createdBy", Session["user"]);

                int rows = insertCommand.ExecuteNonQuery();
                if (rows>0)
                    lblResult.Text = "The user account was successfully created = " + rows;
                lblResult.Text = "The user account was successfully created ";

                myConnection.Close();

                Roles.AddUserToRole(UserName.Text, "Member");
                

                lblResult.ForeColor = Color.Green;
                UserName.Text = string.Empty;
                Email.Text = string.Empty;
                Question.Text = string.Empty;
                Answer.Text = string.Empty;
                FirstName.Text = string.Empty;
                LastName.Text = string.Empty;
                Email.Text = string.Empty;
                UserName.Text = string.Empty;
                Gender.Text = string.Empty;
                DOB.Text = string.Empty;
                Specialisation.Text = string.Empty;

                Session["user"] = Membership.GetUser(UserName.Text);
                Response.Redirect("/");

                break;
            case MembershipCreateStatus.DuplicateUserName:
                lblResult.ForeColor = Color.Red;
                lblResult.Text = "The user with the same UserName already exists!";
                break;
            case MembershipCreateStatus.DuplicateEmail:
                lblResult.ForeColor = Color.Red;
                lblResult.Text = "The user with the same email address already exists!";
                break;
            case MembershipCreateStatus.InvalidEmail:
                lblResult.ForeColor = Color.Red;
                lblResult.Text = "The email address you provided is invalid.";
                break;
            case MembershipCreateStatus.InvalidAnswer:
                lblResult.ForeColor = Color.Red;
                lblResult.Text = "The security answer was invalid.";
                break;
            case MembershipCreateStatus.InvalidPassword:
                lblResult.ForeColor = Color.Red;
                lblResult.Text = "The password you provided is invalid. It must be 7 characters long and have at least 1 special character.";
                break;
            default:
                lblResult.ForeColor = Color.Red;
                lblResult.Text = "There was an unknown error; the user account was NOT created.";
                break;
        }
    }


}
/*
        var manager = new UserManager();
        var user = new ApplicationUser() { UserName = UserName.Text };
        IdentityResult result = manager.Create(user, Password.Text);
        if (result.Succeeded)
        {
            IdentityHelper.SignIn(manager, user, isPersistent: false);
            IdentityHelper.RedirectToReturnUrl(Request.QueryString["ReturnUrl"], Response);
        }
        else
        {
            ErrorMessage.Text = result.Errors.FirstOrDefault();
        }
*/