﻿using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using System;
using System.Web;
using System.Web.UI;
using Assignment;
using System.Web.Security;

public partial class Account_Login : Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        
        if(Session["user"] != null)
        {
            Response.Redirect("/");
        }
        RegisterHyperLink.NavigateUrl = "Register";
    }
    
    protected void LogIn(object sender, EventArgs e) {
        string ReturnUrl = Request.QueryString["ReturnUrl"];
        if (Membership.ValidateUser(UserName.Text, Password.Text))
        {
            Session["user"] = Membership.GetUser(UserName.Text);
            if (!string.IsNullOrEmpty(ReturnUrl)){
                Response.Redirect(ReturnUrl);
            }
            else{
                Response.Redirect("/");
            }
        }
        else
        {
            lblResult.Text = "Invalid Login";
        }
    }
}