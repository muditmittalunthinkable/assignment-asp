﻿<%@ Page Title="Register" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeFile="Register2.aspx.cs" Inherits="Account_Register2" %>

<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="MainContent">
    <style>
        .error {
          color: red;
          margin-left: 5px;
        }
    </style>
    <h2><%: Title %>.</h2>
    <div class="p-5">
        <h4>Create a new account.</h4>
        <hr />

        <form class="p-3" id="form1">
            <div class="form-group">
              <label for="username" class="col-form-label">Username*</label>
              <input type="text" class="form-control" id="username">

            </div>
            <div class="form-group">
              <label for="password" class="col-form-label">Password*</label>
              <input type="text" class="form-control" id="password">
            </div>
            <div class="form-group">
              <label for="firstName" class="col-form-label">First Name*</label>
              <input type="text" class="form-control" id="firstName">
            </div>
            <div class="form-group">
              <label for="lastName" class="col-form-label">Last Name*</label>
              <input type="text" class="form-control" id="lastName">
            </div>
            <div class="form-group">
              <label for="email" class="col-form-label">Email*</label>
              <input type="text" class="form-control" id="email">
            </div>
            <div class="form-group">
              <label for="gender" class="col-form-label">Gender</label>
              <input type="text" class="form-control" id="gender">
            </div>
            <div class="form-group">
              <label for="dob" class="col-form-label">Date of Birth</label>
              <input type="date" class="form-control" id="dob">
            </div>
            <div class="form-group">
              <label for="specialisation" class="col-form-label">Specialisation</label>
              <input type="text" class="form-control" id="specialisation">
            </div>
            <div class="form-group">
              <label for="question" class="col-form-label">Security Question*</label>
              <input type="text" class="form-control" id="question">
            </div>
            <div class="form-group">
              <label for="answer" class="col-form-label">Answer*</label>
              <input type="text" class="form-control" id="answer">
            </div>

            <% if (Session["user"] != null)
                {%>                
                <div class="form-group">
                  <label for="createdBy" class="col-form-label">CreatedBy</label>
                  <input type="text" class="form-control" id="createdBy" disabled value="<%: Session["user"] %>">
                </div>
            <%
                }
               %>

            
            <input type="submit" id="submit_Click" value="Submit" class="btn btn-primary"/>

        </form>

    </div>

    <script>
        $(document).ready(function () {

          $('#submit_Click').click(function(e) {
              e.preventDefault();
              var validatedData = true;
            
              var firstName = $('#firstName').val();
              var lastName = $('#lastName').val();
              var email = $('#email').val();
              var password = $('#password').val();
              var username = $('#username').val();
              var gender = $('#gender').val();
              var dob = $('#dob').val();
              var specialisation = $('#specialisation').val();
              var question = $('#question').val();
              var answer = $('#answer').val();
              var createdBy = $('#createdBy').val();
              if (createdBy == undefined) {
                  createdBy = 'self';
              }
        
              $(".error").remove();
        
              if (username.length < 1) {
                  $('#username').after('<span class="error">This field is required</span>');
                  validatedData = false;
                }
              if (question.length < 1) {
                $('#question').after('<span class="error">This field is required</span>');
                  validatedData = false;
                }
              if (answer.length < 1) {
                $('#answer').after('<span class="error">This field is required</span>');
                  validatedData = false;
              }
              if (firstName.length < 1) {
                $('#firstName').after('<span class="error">This field is required</span>');
                  validatedData = false;
              }
              if (lastName.length < 1) {
                $('#lastName').after('<span class="error">This field is required</span>');
                  validatedData = false;
              }
              if (email.length < 1) {
                $('#email').after('<span class="error">This field is required</span>');
                  validatedData = false;
              } else {
                var regEx = /^\b[A-Z0-9._%-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b$/i;
                var validEmail = regEx.test(email);
                if (!validEmail) {
                  $('#email').after('<span class="error">Enter a valid email</span>');
                  validatedData = false;
                }
              }
              if (password.length < 8) {
                  $('#password').after('<span class="error">Password must be at least 8 characters long </span>');
                  validatedData = false;
              } else {
                var regEx = /^[A-Za-z0-9 ]+$/;
                var invalidPass = regEx.test(password);
                if (invalidPass) {
                  $('#password').after('<span class="error">Password should contain 1 special character</span>');
                  validatedData = false;
                }
              }
              createData = "{'Username': '" + username + "','Password': '" + password + "','Firstname': '" + firstName + "','Lastname': '" + lastName + "','Email': '" + email + "','Gender': '" + gender + "','Dob': '" + dob + "','Specialisation': '" + specialisation + "','Question': '" + question + "','Answer': '" + answer + "','CreatedBy':'" + createdBy + "'}";

              if (validatedData) {
                  console.log('its inside');
                $.ajax({
                    type: "POST",
                    url: "../WebService.asmx/Create",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: createData,
                    success: function (r) {
//                        location.reload();
                        alert(r.d);
                    },
                    error: function (r) {
                        alert('error');
                        console.log(r.responseText);
                    },
                    failure: function (r) {
                        alert('failure');
                        console.log(r.responseText);
                    }
                });

              }


          });
        
        });
    </script>
</asp:Content>